Easily access Trello from the press of a shortcut!

# Shortcuts

This app turns you into a Trello power-user with these shortcuts:
- Show/hide: Ctrl+Alt+X
- Add a card: Ctrl+Alt+C
- Copy the first list to clipboard: Ctrl+Alt+.

# Install

On Linux, install from: https://snapcraft.io/trello-desktop/

You can also install from the CLI with: `snap install trello-desktop`

# Release notes

# 1.0.16 (2024-09-16)

- Fixed enhancements didn't work even when turned on.
- Allowed Okta sign in.

## 1.0.15 (2024-07-29)

- Turned off enhancements by default, added a setting to turn them on again.

## 1.0.9 (2023-06-18)

- Fixed add a card stopped working.

## 1.0.8 (2023-06-18)

- Fixed settings file getting corrupted.
- Added reset app data popup.
- Better dark theme support.

## 1.0.7 (2023-01-29)

- Fixed google sign in.
- Fixed run at startup.
- Changed copy list shortcut to Ctrl+Shift+.
