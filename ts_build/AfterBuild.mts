import 'source-map-support/register.js'; // This is makes error messages use the .ts file path instead. Very handy.
import fs from 'fs-extra';
import path from 'path';

export class AfterBuild
{

    public static moveExecutable(binaryPath: string): string
    {
        var newBinaryPath = `installableBinaries/${path.basename(binaryPath)}`;
        console.log(`   - Moving binary to: ${newBinaryPath}`); // Keep for debugging.
        fs.rename(binaryPath, newBinaryPath, function (errorObj: Error): void
        {
            if (errorObj) {
                throw errorObj;
            }
        });
        return newBinaryPath;
    }

    public static deleteDistFolder(): void
    {
        console.log(`   - Deleting /dist folder.`); // Keep for debugging.
        fs.rm(`dist`, {recursive: true});
    }

}
