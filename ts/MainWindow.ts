import {BrowserWindow, Menu, screen, shell} from 'electron';
import {MainApp} from './MainApp';
import {MenuBar} from './MenuBar';
import {TrayIcon} from './TrayIcon';
import path = require('path');

export class MainWindow
{

    public static isFreezingWindowOrder: boolean;
    public static unfreezeWindowOrderTimeout: ReturnType<typeof setTimeout>;
    public static windowsArr: BrowserWindow[] = []; // Sorted by last focus, focusing puts the window at the beginning. E.g., `MainWindow.windowsArr[0]` gets the last focused window.
    public static defaultWidthPx              = 1460;
    public static defaultHeightPx             = 800;

    public static init(): void
    {
        var windowPropertiesArr = MainApp.electronStoreObj.get(`windowPropertiesArr`);
        if (windowPropertiesArr.length) {
            windowPropertiesArr.forEach(function (windowPropertiesObj): void
            {
                MainWindow.createWindow(windowPropertiesObj);
            });
        } else {
            MainWindow.createWindow({
                'pageUrl':   MainApp.applicationUrl,
                'boundsObj': MainWindow.getCenteredWindowBounds(MainWindow.defaultWidthPx, MainWindow.defaultHeightPx),
            });
        }
    }

    public static createWindow(windowPropertiesObj: WindowProperties): void
    {
        var mainWin = new BrowserWindow({
            title:          MainApp.packageJsonObj.productName,
            x:              windowPropertiesObj.boundsObj.x,
            y:              windowPropertiesObj.boundsObj.y,
            width:          windowPropertiesObj.boundsObj.width,
            height:         windowPropertiesObj.boundsObj.height,
            icon:           MainApp.appIconPath,
            webPreferences: { // Setting `contextIsolation` here breaks the Trello login flow.
                preload:         path.join(__dirname, `MainWindowPreload.js`),
                devTools:        !MainApp.isPackaged,
                nodeIntegration: true,
            }
        });
        mainWin.setMenuBarVisibility(false);
        if (windowPropertiesObj.pageUrl && windowPropertiesObj.pageUrl.startsWith(MainApp.applicationUrl)) {
            mainWin.loadURL(windowPropertiesObj.pageUrl);
        } else {
            mainWin.loadURL(MainApp.applicationUrl);
        }
        MainWindow.setupWindowEvents(mainWin);
        MainWindow.windowsArr.push(mainWin);
    }

    private static setupWindowEvents(mainWin: BrowserWindow): void
    {
        mainWin.on(`close`, function (evt): void
        {
            if (!MainApp.isQuitting) {
                if (MainWindow.windowsArr.length > 1) {
                    MainWindow.windowsArr.splice(MainWindow.windowsArr.indexOf(mainWin), 1); // Remove from `MainWindow.windowsArr`.
                    MainWindow.setWindowPropertiesArr();
                    TrayIcon.setTrayMenu();
                } else { // Don't allow closing of last window.
                    evt.preventDefault();
                    mainWin.hide();
                }
            }
        });
        mainWin.on(`focus`, function (): void
        {
            if (!MainWindow.isFreezingWindowOrder) {
                // Move window to beginnining of `MainWindow.windowsArr`.
                MainWindow.moveWindowToStartOfWindowsArr(mainWin);
                MainWindow.setWindowPropertiesArr();
            }
        });
        mainWin.on(`leave-full-screen`, function (): void
        {
            mainWin.setMenuBarVisibility(false);
        });
        // @formatter:off
        mainWin.on(`page-title-updated`,               function (evt, titleStr             ): void { TrayIcon.setTrayMenu(titleStr, mainWin.id);                  }); // Pass in `titleStr` because `.getTitle()` hasn't been updated at this point.
        mainWin.on(`move`,                             function (                          ): void { MainWindow.setWindowPropertiesArr();                         });
        mainWin.on(`resize`,                           function (                          ): void { MainWindow.setWindowPropertiesArr();                         });
        mainWin.webContents.on(`did-navigate`,         function (                          ): void { MainWindow.setWindowPropertiesArr(); TrayIcon.setTrayMenu(); });
        mainWin.webContents.on(`did-navigate-in-page`, function (                          ): void { MainWindow.setWindowPropertiesArr(); TrayIcon.setTrayMenu(); });
        // @formatter:on
        mainWin.webContents.on(`context-menu`, function (): void
        {
            // On Mac (darwin) it doesn`t focus the window if you right-click on it.
            if (MainApp.platformType == `mac`) {
                mainWin.focus();
            }
            Menu.buildFromTemplate(MenuBar.generateRightClickMenu()).popup({});
        });
        mainWin.webContents.setWindowOpenHandler(function (detailsObj): { action: `deny` }
        {
            if (detailsObj.url.startsWith(MainApp.applicationUrl)) {
                MainWindow.createWindow({
                    'pageUrl':   detailsObj.url,
                    'boundsObj': MainWindow.getCenteredWindowBounds(MainWindow.defaultWidthPx, MainWindow.defaultHeightPx),
                });
            } else {
                shell.openExternal(detailsObj.url);
            }
            return {action: `deny`};
        });
        mainWin.webContents.on(`will-navigate`, function (evt, navigateUrl): void
        {
            if (!MainApp.urlContainsAllowlistArr.some((urlContainsStr): boolean => navigateUrl.includes(urlContainsStr))) {
                shell.openExternal(navigateUrl);
                evt.preventDefault();
            }
        });
        mainWin.webContents.ipc.handle(`isAddEnhancements`, async function (): Promise<boolean>
        {
            return MainApp.electronStoreObj.get(`isAddEnhancements`);
        });
    }

    public static moveWindowToStartOfWindowsArr(mainWin: BrowserWindow): void
    {
        var winIndexNum = MainWindow.windowsArr.indexOf(mainWin);
        if (winIndexNum != 0) {
            MainWindow.windowsArr.splice(winIndexNum, 1);
            MainWindow.windowsArr.unshift(mainWin);
        }
    }

    public static getCenteredWindowBounds(widthPx: number, heightPx: number): Electron.Rectangle
    {
        var mainScreenBoundsObj = screen.getDisplayNearestPoint(screen.getCursorScreenPoint()).bounds;
        var winX                = mainScreenBoundsObj.x + ((mainScreenBoundsObj.width - widthPx) / 2);
        var winY                = mainScreenBoundsObj.y + ((mainScreenBoundsObj.height - heightPx) / 2);
        return {
            'width':  widthPx,
            'height': heightPx,
            'x':      Math.round(winX), // Math.round needed since it throws a strange error without it.
            'y':      Math.round(winY) // Math.round needed since it throws a strange error without it.
        };
    }

    public static runOnAllWindows(runFunc: (indexNum: number, mainWin: BrowserWindow) => void): void
    {
        for (var i = MainWindow.windowsArr.length - 1; i >= 0; i--) {
            if (!MainWindow.windowsArr[i].isDestroyed()) {
                runFunc(i, MainWindow.windowsArr[i]);
            }
        }
    }

    /*
     * Save all the window's state, for app re-opening.
     * Can't call in `before-quit` because `before-quit` isn't triggered from a Ctrl+C in the terminal.
     */
    public static setWindowPropertiesArr(): void
    {
        var windowPropertiesArr: WindowProperties[] = [];
        MainWindow.runOnAllWindows(function (indexNum: number, mainWin: BrowserWindow): void
        {
            windowPropertiesArr.push(
                {
                    'pageUrl':   mainWin.webContents.getURL(),
                    'boundsObj': mainWin.getBounds(),
                }
            );
        });
        MainApp.electronStoreObj.set(`windowPropertiesArr`, windowPropertiesArr);
    }

    public static showHideMainWin(isShow?: boolean): void
    {
        if (
            ( // If `isShow` is passed in, use that instead.
                isShow !== undefined
                &&
                isShow
            )
            ||
            (
                isShow === undefined
                &&
                (
                    !MainWindow.windowsArr[0].isVisible()
                    ||
                    !MainWindow.windowsArr[0].isFocused()
                )
            )
        ) {
            MainWindow.isFreezingWindowOrder = true;
            MainWindow.runOnAllWindows(function (indexNum: number, mainWin: BrowserWindow): void
            {
                // Show will focus the window, sometimes (not reliable).
                // The `focus` event is fired asynchronously, and sometimes in a different order (not reliable).
                mainWin.show(); // Can't use `showInactive` here because you'd get "app is ready" notification on Ubuntu 20.04.
            });
            MainWindow.windowsArr[0].focus();

            // Using a `setTimeout` is the only way I've found to reliably show and hide all the windows in the correct order.
            clearTimeout(MainWindow.unfreezeWindowOrderTimeout);
            MainWindow.unfreezeWindowOrderTimeout = setTimeout(function (): void
            {
                MainWindow.isFreezingWindowOrder = false;
            }, 400);
        } else {
            MainWindow.runOnAllWindows(function (indexNum: number, mainWin: BrowserWindow): void
            {
                mainWin.hide();
            });
        }
    }

}
