interface Point
{
    x: number,
    y: number,
}

interface WindowProperties
{
    pageUrl: string,
    boundsObj: Electron.Rectangle,
}

interface DefaultSettings
{
    defaultListNum: number,
    isRunAtStartup: boolean,
    isAddEnhancements: boolean,
    windowPropertiesArr: WindowProperties[],
}
