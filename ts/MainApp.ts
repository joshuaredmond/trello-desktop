import 'source-map-support/register.js'; // This is makes error messages use the .ts file path instead. Very handy.

import fs from 'fs-extra';
import path from 'path';
import ElectronStore from 'electron-store';
import Rollbar from 'rollbar';
import {ipcMain, clipboard, globalShortcut, app, BrowserWindow} from 'electron';
import {SettingsWindow} from './SettingsWindow';
import {MainWindow} from './MainWindow';
import {MenuBar} from './MenuBar';
import {TrayIcon} from './TrayIcon';

// @ts-ignore because this file is outside the ts_build folder.
// Allow eslint-disable because eslint hasn't caught up to the new assert syntax.
// eslint-disable-next-line @typescript-eslint/quotes
import packageJsonObj from '../../package.json' assert {type: 'json'};

export class MainApp
{

    public static shortcutStr_addCard: string      = `CmdOrCtrl+Alt+C`;
    public static shortcutStr_showHide: string     = `CmdOrCtrl+Alt+X`;
    public static shortcutStr_copyDoneList: string = `CmdOrCtrl+Alt+.`;
    public static platformType: string;
    public static iconExtStr: string;
    public static appPath: string;
    public static isPackaged: boolean;
    public static appIconPath: string;
    public static packageJsonObj: {
        name: string;
        productName: string;
        version: string;
    };
    public static rollbarObj: Rollbar;
    public static electronStoreObj: ElectronStore<DefaultSettings>;
    public static isQuitting                       = false;
    public static applicationUrl                   = `https://trello.com`;
    public static urlContainsAllowlistArr     = [
        MainApp.applicationUrl,
        `atlassian`,
        `https://accounts.google.com/`,
        `okta.com`,
    ];


    public static init(): void
    {
        MainApp.initErrorReporting();
        if (process.platform == `win32`) {
            MainApp.platformType = `win`;
            MainApp.iconExtStr   = `ico`;
        } else if (process.platform == `darwin`) {
            MainApp.platformType = `mac`;
            MainApp.iconExtStr   = `icns`;
        } else {
            MainApp.platformType = `lin`;
            MainApp.iconExtStr   = `png`;
        }
        MainApp.appPath          = path.join(__dirname, `..`);
        MainApp.appIconPath      = path.join(MainApp.appPath, `assets`, `icon.${MainApp.iconExtStr}`);
        MainApp.electronStoreObj = new ElectronStore<DefaultSettings>({
            'name':               `trelloDesktopConfig`,
            'clearInvalidConfig': true,
            'cwd':                process.env.SNAP_USER_COMMON || app.getPath(`userData`), // The snap common path stores data that's shared between all versions, so this way an update will keep the settings of an older version.
            'defaults':           <DefaultSettings>{
                'defaultListNum':      3,
                'isRunAtStartup':      false,
                'isAddEnhancements':   false,
                'windowPropertiesArr': [],
            }
        });

        MainApp.setRunAtStartup(MainApp.electronStoreObj.get(`isRunAtStartup`));
        MainApp.setupAppEvents();
        MainApp.setupIpc();
    }

    public static setupAppEvents(): void
    {
        app.on(`ready`, function (): void
        {
            MainApp.onReady();
        });
        app.on(`window-all-closed`, function (): void
        {
            if (MainApp.platformType != `mac`) {
                app.quit();
            }
        });
        app.on(`before-quit`, function (): void
        {
            MainApp.beforeQuit();
        });
        MainApp.logMessageOnConsoleMessage();
    }

    private static logMessageOnConsoleMessage(): void
    {
        app.on(`web-contents-created`, function (evt: Electron.Event, webContentsObj: Electron.WebContents): void
        {
            webContentsObj.on(`console-message`, function (evt: Electron.Event, levelNum: number, messageStr: string): void
            {
                if (!messageStr.includes(`Electron Security Warning (Insecure Content-Security-Policy)`)) {
                    var levelStr = ``;
                    if (levelNum == 0) {
                        levelStr = `verbose`;
                    } else if (levelNum == 1) {
                        levelStr = `info`;
                    } else if (levelNum == 2) {
                        levelStr = `warning`;
                    } else if (levelNum == 3) {
                        levelStr = `error`;
                    }
                    console.log(`${webContentsObj.getTitle()} ${levelStr}: ${messageStr.replaceAll(`\n`, `\n    `)}`); // Keep for debugging.
                }
            });
        });
    }

    private static initErrorReporting(): void
    {
        MainApp.isPackaged     = app.isPackaged;
        MainApp.packageJsonObj = packageJsonObj;

        // Error reporting.
        if (MainApp.isPackaged) {
            MainApp.rollbarObj = new Rollbar({
                accessToken:                `dd2f4f1d63ac40129c8d412ef77a878d`,
                captureUncaught:            true,
                captureUnhandledRejections: true,
                codeVersion:                MainApp.packageJsonObj.version,
                environment:                (MainApp.isPackaged) ? `production` : `development`, // Will always be `production`, except when I enable it on dev for testing.
            });
        }
    }

    private static onReady(): void
    {
        MainApp.setupSingleInstanceLock();
        MenuBar.init();
        MainWindow.init();
        SettingsWindow.init();
        TrayIcon.init();
        MainApp.registerShortcuts();
    }

    public static beforeQuit(): void
    {
        MainApp.isQuitting = true;
        MainWindow.setWindowPropertiesArr();
        TrayIcon.trayIconObj.destroy();
    }

    public static setRunAtStartup(isRunAtStartup: boolean): void
    {
        if (MainApp.isPackaged) {
            if (MainApp.platformType == `lin`) {
                MainApp.setRunAtStartupLin(isRunAtStartup);
            }
            // Windows run at startup isn't built yet. See: `mod/electron/desktop/!Readme - Windows.md:33` (Look for "Run at startup". To open: [_Alt_]+[_Shift_]+[_N_])
        }
    }

    private static setRunAtStartupLin(isRunAtStartup: boolean): void
    {
        var autostartDesktopFilePath = `${process.env.SNAP_USER_DATA || process.env.HOME}/.config/autostart/${MainApp.packageJsonObj.name}.desktop`; // For some reason `os.userInfo().homedir` doesn't get the snap's home folder when installed from snap.
        var originalDesktopFilePath  = ``;

        if (MainApp.electronStoreObj.get(`desktopFilePath`)) {
            originalDesktopFilePath = MainApp.electronStoreObj.get(`desktopFilePath`);
        } else {
            if (process.env.BAMF_DESKTOP_FILE_HINT) { // Snap sets this environment variable when the app is run from a .desktop file.
                // For .snap install.
                originalDesktopFilePath = process.env.BAMF_DESKTOP_FILE_HINT;
            } else {
                // For .deb install.
                originalDesktopFilePath = `/usr/share/applications/${MainApp.packageJsonObj.name}.desktop`;
            }
            MainApp.electronStoreObj.set(`desktopFilePath`, originalDesktopFilePath);
        }

        fs.pathExists(originalDesktopFilePath, function (errorObj: Error, isOriginalDesktopFileExists: boolean): void
        {
            if (errorObj) {
                MainApp.electronStoreObj.set(`isRunAtStartup`, false);
                throw errorObj;
            } else if (isOriginalDesktopFileExists) {
                fs.pathExists(autostartDesktopFilePath, function (errorObj: Error, isAutostartDesktopFileExists: boolean): void
                {
                    if (errorObj) {
                        MainApp.electronStoreObj.set(`isRunAtStartup`, false);
                        throw errorObj;
                    } else {
                        if (isRunAtStartup) {
                            if (!isAutostartDesktopFileExists) {
                                fs.ensureSymlink(originalDesktopFilePath, autostartDesktopFilePath, function (errorObj: Error): void
                                {
                                    if (errorObj) {
                                        MainApp.electronStoreObj.set(`isRunAtStartup`, false); // Reset the setting so they can try again.
                                        throw errorObj;
                                    }
                                });
                            }
                        } else {
                            if (isAutostartDesktopFileExists) {
                                fs.unlink(autostartDesktopFilePath, function (errorObj: Error): void
                                {
                                    if (errorObj) {
                                        MainApp.electronStoreObj.set(`isRunAtStartup`, true); // Reset the setting so they can try again.
                                        throw errorObj;
                                    }
                                });
                            }
                        }
                    }
                });
            }
        });
    }

    /*
     * When a second instance is opened quit the second instance and focus the first instance.
     */
    public static setupSingleInstanceLock(): void
    {
        var isOnlyInstance = app.requestSingleInstanceLock();
        if (isOnlyInstance) {
            app.on(`second-instance`, function (): void
            {
                MainWindow.showHideMainWin(true);
            });
        } else {
            app.quit();
        }
    }

    public static registerShortcuts(): void
    {
        globalShortcut.register(MainApp.shortcutStr_addCard, function (): void
        {
            MainWindow.showHideMainWin(true);
            MainWindow.windowsArr[0].webContents.send(`addCard`, MainApp.electronStoreObj.get(`defaultListNum`));
        });
        globalShortcut.register(MainApp.shortcutStr_showHide, function (): void
        {
            MainWindow.showHideMainWin();
        });
        globalShortcut.register(MainApp.shortcutStr_copyDoneList, function (): void
        {
            MainWindow.windowsArr[0].webContents.send(`get1stListText`);
        });
    }

    public static setupIpc(): void
    {
        ipcMain.on(`hideSettingsWin`, function (): void
        {
            SettingsWindow.settingsWin.hide();
        });
        ipcMain.on(`copy1stListText`, function (ipcEvent, listText): void
        {
            clipboard.writeText(listText);
        });
        ipcMain.on(`setStoreOption`, function (ipcEvent, optionObj): void
        {
            MainApp.electronStoreObj.set(optionObj.itemKeyStr, optionObj.itemValueMixed);
            if (MainApp.isPackaged && optionObj.itemKeyStr == `isRunAtStartup`) {
                MainApp.setRunAtStartup(optionObj.itemValueMixed);
            }
            if (optionObj.itemKeyStr == `isAddEnhancements`) {
                MainWindow.runOnAllWindows(function (indexNum: number, mainWin: BrowserWindow): void
                {
                    mainWin.reload();
                });
            }
        });
    }

}

MainApp.init();
