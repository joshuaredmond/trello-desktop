import path from 'path';
import {BrowserWindow} from 'electron';
import {MainApp} from './MainApp';
import {MainWindow} from './MainWindow';

export class SettingsWindow
{

    public static settingsWin: BrowserWindow;

    public static init(): void
    {
        var isDebug                = false;
        var widthPx                = (isDebug) ? 1500 : 338;
        var heightPx               = (isDebug) ? 1000 : 270;
        var boundsObj              = MainWindow.getCenteredWindowBounds(widthPx, heightPx);
        SettingsWindow.settingsWin = new BrowserWindow({
            width:          boundsObj.width,
            height:         boundsObj.height,
            x:              boundsObj.x,
            y:              boundsObj.y,
            resizable:      isDebug,
            show:           false,
            icon:           MainApp.appIconPath,
            title:          `Settings`,
            webPreferences: {
                devTools: isDebug,
                preload:  path.join(__dirname, `SettingsWindowPreload.js`),
            },
        });
        SettingsWindow.settingsWin.removeMenu();
        SettingsWindow.settingsWin.on(`ready-to-show`, function (): void
        {
            SettingsWindow.settingsWin.webContents.send(`settingsObj`, MainApp.electronStoreObj.get(`defaultListNum`), MainApp.electronStoreObj.get(`isRunAtStartup`), MainApp.electronStoreObj.get(`isAddEnhancements`), MainApp.packageJsonObj.version);
        });
        if (isDebug) {
            SettingsWindow.settingsWin.on(`resize`, function (): void
            {
                console.log(SettingsWindow.settingsWin.getSize()); // Allow `console.log`.
            });
        }
        SettingsWindow.settingsWin.on(`show`, function (): void
        {
            SettingsWindow.settingsWin.setBounds(MainWindow.getCenteredWindowBounds(widthPx, heightPx));
            SettingsWindow.settingsWin.setAlwaysOnTop(true); // Using `alwaysOnTop` doesn't work, calling this function outside `show` doesn't work either.
        });
        SettingsWindow.settingsWin.on(`close`, function (evt): void
        {
            if (!MainApp.isQuitting) {
                evt.preventDefault();
                SettingsWindow.settingsWin.hide();
            }
        });
        SettingsWindow.settingsWin.webContents.on(`will-navigate`, function (evt): void
        { // To stop settings window from reloading.
            evt.preventDefault();
        });
        SettingsWindow.settingsWin.loadURL(`file://${__dirname}/../settingsWindow.html`);
    }

}
