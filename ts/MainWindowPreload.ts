import {IpcRendererEvent, ipcRenderer} from 'electron';
import fs from 'fs';
import path from 'path';

window.addEventListener(`load`, function (): void
{
    const $ = require(`jquery`);
    ipcRenderer.invoke(`isAddEnhancements`).then(function (isAddEnhancements: boolean): void
    {
        if (isAddEnhancements) {
            addEnhancements();
        }
    });
    ipcRenderer.on(`addCard`, function (evt: IpcRendererEvent, defaultListNum: number): void
    {
        // New.
        const $cardTextareaJq = $(`#board [data-testid="list-wrapper"]:nth-child(${defaultListNum}) [data-testid="list-card-composer-textarea"]`);
        if ($cardTextareaJq.length) { // If composer is open already, then focus.
            $cardTextareaJq.get(0).focus();
        } else {
            var $addCardButtonJq = $(`#board [data-testid="list-wrapper"]:nth-child(${defaultListNum}) [data-testid="list-add-card-button"]`);
            if (!$addCardButtonJq.length) { // Add to last list if default list isn't found.
                $addCardButtonJq = $(`#board [data-testid="list-wrapper"]:last [data-testid="list-add-card-button"]`);
            }
            $addCardButtonJq.get(0).click();
        }

        // Old.
        // var $addCardButtonJq = $(`#board .js-list:nth-child(${defaultListNum}) .list .open-card-composer`);
        // if (!$addCardButtonJq.length) { // Add to last list if default list isn't found.
        //     $addCardButtonJq = $(`#board .js-list:last .list .open-card-composer`);
        // }

        // // If you can add a card then press the button.
        // if ($addCardButtonJq.is(`:visible`)) {
        //     $addCardButtonJq.get(0).click();
        // } else { // Or if the button has already been pressed then just focus the textarea.
        //     $(`.list-card-composer-textarea`).get(0).focus();
        // }
    });
    ipcRenderer.on(`get1stListText`, function (): void
    {
        // New.
        var listText     = ``;
        const $firstListWrapperJq = $(`[data-testid="list-wrapper"]:first`);
        var titleStr     = `Nothing to copy`;
        var listHeading  = $firstListWrapperJq.find(`[data-testid="list-name"]`).text();
        if (listHeading) {
            titleStr = `Copied "${listHeading}"`;
            let cardCount = 1;
            $firstListWrapperJq.find(`[data-testid='card-name']`).each(function (i: number, cardEl: HTMLElement): void
            {
                listText += `${cardCount++}. ${cardHeading($(cardEl).parent().get(0))}\n`;
            });
            ipcRenderer.send(`copy1stListText`, listText);
        }

        // Old.
        // var listText     = ``;
        // var $firstListJq = $(`.list:first`);
        // var titleStr     = `Nothing to copy`;
        // var listHeading  = $firstListJq
        //     .find(`.list-header`)
        //     .first()
        //     .find(`h2`)
        //     .first()
        //     .html();
        // if (listHeading) {
        //     titleStr = `Copied "${listHeading}"`;
        //     let cardCount = 1;
        //     $firstListJq.find(`.list-card-details`).each(function (i: number, cardEl: HTMLElement): void
        //     {
        //         listText += `${cardCount++}. ${cardHeading(cardEl)}\n`;
        //     });
        //     ipcRenderer.send(`copy1stListText`, listText);
        // }
        new Notification(titleStr, {
            // Icon is base64 of: `icon32x32 (converted to base64 for notifications).png`
            icon: `data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAMAAABEpIrGAAABd1BMVEUAAAAjgPsjgfwDVc4DVc8jgPsjgPsDVc8DVtAjgPskgv0DVtACVc8kgfwjgPsDVtEQcu4TdPAUdfEWd/NBfe4BVM4kgv1MfuhGhve5xfHo7P3s8P0kgv1Ug+pVg+tVhe1XjfdhgN28y/rj5/kBVM4devUhfvkQaeMee/YuZ9vCz/oEV9IQaeMee/Ykgv06a9qbqubU2vP6+v0BVM4CVdADVtAEV9IFWNMFWdMGWtUGW9UHXNYHXdcIXtgJX9oKYNsLYNsLYdsLYdwMYt0OZN8PZeAQZ+EQZ+IQauQRaeMSauQTbOYTbOcUbegUbukVbukWcOsXcOsYcu0Zc+4adfAbdfAbdvEbd/IdePMdefQdevUeevUfe/YffPcgffghffghfvkhf/ojgPskgv1fgOBfgeFfguNgg+RgheVghuZghudgiOlgiethiexhi+5hjO9hjfBijvJij/NikPWjsOfo6vno6/no6/ro6/vo7Pvo7Pzo7P3///8J3cRZAAAAM3RSTlMANzc6Oz4/QkS+vsDDw8fN8PDw8fb3+Pj5+fn5+vr6+vr6+vr7/Pz9/f39/v7+/v7+/v4ghur3AAABYUlEQVQ4y33KB1fCMBQF4OteuBfuvRX3AhmKigpuqKgVQdx7g8CPNy9NPEJP+dLT5L53gYJyS9SApTQXKGyNZtGQg4qLrEoQDkfYCadfkb+rH6E056HMDFU9Y2ewk5hVnsw8jNFcVXFK+lpSXDeFYe3dfMJXCDLHH9oslaSQFKGWVkEckXcxS1BIiFBDQYFfCfiVN1kIKH5FFqoDBIfkVRYoyEIVX+GAvMgCBVmo4yvsk2cx+6EQF6GDwh52yZOYxSnIQjtfYZs8ysIOC7LQxlfwMr4HWfD6fN6YCI0+xostci9mMQpN2rtng6+wSe5kgafRAbKpgcfjWffcysI6w/I/WCM3ovC9poNVci0LqzpwkytZcOvASS5F4cu1vOJyOQUXgYMMTWmFSYcO7FzXJ5kYt+vAplmyGYDVal3knwEsMPOM/GcmzGU3gsrZDDPy5o8yFPdOZ1GfBxSZjPemfPwCSEpHU6FuQrkAAAAASUVORK5CYII=`,
            body: listText,
        });
    });

    // New.
    // Taken from: ZJS.Crx.Trello.cardHeading
    function cardHeading(cardEl: HTMLElement): string
    {
        var $cardHeadingJq = $(cardEl).find(`a[data-testid='card-name']`).first();
        var cardHeading    = $cardHeadingJq.text();
        if (cardHeading) {
            if (cardHeading.startsWith(`#`)) {
                var cardId  = $cardHeadingJq
                    .find(`span`)
                    .first()
                    .text();
                cardHeading = cardHeading.replace(cardId, ``);
            }
            var cardCountStr = $cardHeadingJq
                .find(`.cardCounter_appsBrand`)
                .last()
                .text();
            if (cardCountStr) {
                cardHeading = cardHeading.replace(new RegExp(`${cardCountStr}$`), ``);
            }
            return cardHeading;
        } else {
            return ``;
        }
    }

    // Old.
    // // Taken from: ZJS.Crx.Trello.cardHeading
    // function cardHeading(cardEl: HTMLElement): string
    // {
    //     var $cardHeadingJq = $(cardEl).find(`.list-card-title`).first();
    //     var cardHeading    = $cardHeadingJq.text();
    //     if (cardHeading) {
    //         if (cardHeading.startsWith(`#`)) {
    //             var cardId  = $cardHeadingJq
    //                 .find(`span`)
    //                 .first()
    //                 .text();
    //             cardHeading = cardHeading.replace(cardId, ``);
    //         }
    //         var cardCountStr = $cardHeadingJq
    //             .find(`.cardCounter_appsBrand`)
    //             .last()
    //             .text();
    //         if (cardCountStr) {
    //             cardHeading = cardHeading.replace(new RegExp(`${cardCountStr}$`), ``);
    //         }
    //         return cardHeading;
    //     } else {
    //         return ``;
    //     }
    // }
});

function addEnhancements(): void
{
    fs.readFile(path.join(__dirname, `..`, `ext`, `generated_colors_lgt.css`), `utf8`, function (errorObj, trelloStyleCss): void
    {
        if (errorObj) {
            return console.log(errorObj);
        } else {
            var styleEl = document.createElement(`style`);

            document.head.appendChild(styleEl);
            styleEl.appendChild(document.createTextNode(trelloStyleCss));
        }
    });
    fs.readFile(path.join(__dirname, `..`, `ext`, `trello.css`), `utf8`, function (errorObj, trelloStyleCss): void
    {
        if (errorObj) {
            return console.log(errorObj);
        } else {
            var styleEl = document.createElement(`style`);

            document.head.appendChild(styleEl);
            styleEl.appendChild(document.createTextNode(trelloStyleCss));
        }
    });

    fs.readFile(path.join(__dirname, `..`, `ext`, `TrelloInject.js`), `utf8`, function (errorObj, trelloJs): void
    {
        if (errorObj) {
            return console.log(errorObj);
        } else {
            var scriptEl = document.createElement(`script`);
            document.head.appendChild(scriptEl);
            scriptEl.appendChild(document.createTextNode(trelloJs));
        }
    });

    fs.readFile(path.join(__dirname, `..`, `ext`, `Trello.js`), `utf8`, function (errorObj, trelloJs): void
    {
        if (errorObj) {
            return console.log(errorObj);
        } else {
            var scriptEl = document.createElement(`script`);
            document.head.appendChild(scriptEl);
            scriptEl.appendChild(document.createTextNode(trelloJs));
        }
    });
}
