import {app, session, BrowserWindow, shell, Menu, dialog} from 'electron';
import {MainApp} from './MainApp';
import {MainWindow} from './MainWindow';
import {SettingsWindow} from './SettingsWindow';
import path = require('path');

export class MenuBar
{

    private static addCardMenuItemObj: Electron.MenuItemConstructorOptions;
    private static settingsMenuItemObj: Electron.MenuItemConstructorOptions;
    private static copy1stListMenuItemObj: Electron.MenuItemConstructorOptions;
    private static showHideMenuItemObj: Electron.MenuItemConstructorOptions;
    private static quitMenuItemObj: Electron.MenuItemConstructorOptions;
    private static resetAppDataItemObj: Electron.MenuItemConstructorOptions;
    private static reportIssueItemObj: Electron.MenuItemConstructorOptions;

    public static init(): void
    {
        MenuBar.addCardMenuItemObj     = {
            label:               `Add card`,
            accelerator:         MainApp.shortcutStr_addCard,
            registerAccelerator: false,
            icon:                path.join(MainApp.appPath, `assets`, `menuIcons`, `new.png`),
            click:               function (): void
                                 {
                                     MainWindow.showHideMainWin(true);
                                     MainWindow.windowsArr[0].webContents.send(`addCard`, MainApp.electronStoreObj.get(`defaultListNum`));
                                 }
        };
        MenuBar.settingsMenuItemObj    = {
            label:       `Settings`,
            accelerator: `CmdOrCtrl+,`,
            icon:        path.join(MainApp.appPath, `assets`, `menuIcons`, `settings.png`),
            click:       function (): void
                         {
                             SettingsWindow.settingsWin.show();
                         }
        };
        MenuBar.copy1stListMenuItemObj = {
            label:               `Copy first list`,
            accelerator:         MainApp.shortcutStr_copyDoneList,
            registerAccelerator: false,
            icon:                path.join(MainApp.appPath, `assets`, `menuIcons`, `copy.png`),
            click:               function (): void
                                 {
                                     MainWindow.windowsArr[0].webContents.send(`get1stListText`);
                                 }
        };
        MenuBar.showHideMenuItemObj    = {
            label:               `Show / hide`,
            accelerator:         MainApp.shortcutStr_showHide,
            registerAccelerator: false,
            click:               function (): void
                                 {
                                     MainWindow.showHideMainWin();
                                 }
        };
        MenuBar.quitMenuItemObj        = {
            label: `Quit`,
            click: function (): void
                   {
                       app.quit();
                   }
        };
        MenuBar.resetAppDataItemObj    = {
            label: `Reset app data`,
            click: function (): void
                   {
                       dialog.showMessageBox(
                            {
                                type:      `question`,
                                buttons:   [`Yes ... reset`, `Cancel`],
                                detail:    `All your preferences are permanently wiped, you'll be logged out, and Trello Desktop restarts.`,
                                defaultId: 0,
                                cancelId:  1,
                                message:   `Reset app data?`,
                                icon:      MainApp.appIconPath, // On Linux this icon looks blurry on a high density display. Couldn't fix it.
                            }
                        ).then(
                            function (responseObj: Electron.MessageBoxReturnValue): void
                            {
                                if (responseObj.response == 0) {
                                    MainApp.electronStoreObj.clear();

                                    session.defaultSession.clearCache();
                                    session.defaultSession.clearStorageData();

                                    app.relaunch();
                                    app.exit();
                                }
                            }
                        );
                   }
        };
        MenuBar.reportIssueItemObj     = {
            label: `Report issue...`,
            click: function (): void
                   {
                       shell.openExternal(`https://gitlab.com/joshuaredmond/trello-desktop/-/issues`);
                   }
        };
        Menu.setApplicationMenu(Menu.buildFromTemplate(MenuBar.getApplicationMenu()));
    }

    private static getWindowsMenuItemsArr(overrideTitleStr?: string, overrideTitleWinId?: number): Electron.MenuItemConstructorOptions[]
    {
        // Add a menu item for each open window.
        var windowsMenuItemsArr: Electron.MenuItemConstructorOptions[] = [];
        MainWindow.windowsArr.forEach(function (mainWin): void
        {
            var labelStr;
            if (overrideTitleStr && overrideTitleWinId == mainWin.id) { // Pass in `titleStr` because `.getTitle()` hasn't been updated at this point.
                labelStr = overrideTitleStr;
            } else {
                labelStr = mainWin.getTitle();
            }
            labelStr = labelStr.replace(` | Trello`, ``);
            labelStr ||= `Trello`;
            windowsMenuItemsArr.push(
                {
                    label: labelStr,
                    icon:  path.join(MainApp.appPath, `assets`, `menuIcons`, `logo.png`),
                    click: function (): void
                           {
                               MainWindow.moveWindowToStartOfWindowsArr(mainWin);
                               MainWindow.setWindowPropertiesArr();
                               MainWindow.showHideMainWin(true);
                           }
                },
            );
        });
        // Sort alphabetically.
        windowsMenuItemsArr.sort(function (aObj, bObj): number
        {
            return (aObj.label || ``).localeCompare(bObj.label || ``);
        });
        windowsMenuItemsArr.push(
            {
                label: `New window...`,
                icon:  path.join(MainApp.appPath, `assets`, `menuIcons`, `add.png`),
                click: function (): void
                       {
                           MainWindow.createWindow({
                               'pageUrl':   MainApp.applicationUrl,
                               'boundsObj': MainWindow.getCenteredWindowBounds(MainWindow.defaultWidthPx, MainWindow.defaultHeightPx),
                           });
                       }
            },
        );
        return windowsMenuItemsArr;
    }

    public static getTrayIconMenu(overrideTitleStr?: string, overrideTitleWinId?: number): Electron.MenuItemConstructorOptions[]
    {
        return [
            MenuBar.addCardMenuItemObj,
            MenuBar.copy1stListMenuItemObj,
            MenuBar.showHideMenuItemObj,
            {type: `separator`},
            ...MenuBar.getWindowsMenuItemsArr(overrideTitleStr, overrideTitleWinId),
            {type: `separator`},
            MenuBar.settingsMenuItemObj,
            MenuBar.reportIssueItemObj,
            MenuBar.resetAppDataItemObj,
            MenuBar.quitMenuItemObj,
        ];
    }

    public static generateRightClickMenu(): Electron.MenuItemConstructorOptions[]
    {
        return [
            ...MenuBar.getWindowsMenuItemsArr(),
            {type: `separator`},
            MenuBar.addCardMenuItemObj,
            MenuBar.copy1stListMenuItemObj,
            MenuBar.showHideMenuItemObj,
            {
                label: `Trello Desktop`,
                submenu:
                       [
                           MenuBar.settingsMenuItemObj,
                           MenuBar.reportIssueItemObj,
                           MenuBar.resetAppDataItemObj,
                       ]
            },
        ];
    }

    public static getApplicationMenu(): Electron.MenuItemConstructorOptions[]
    {
        var applicationMenuArr: Electron.MenuItemConstructorOptions[] = [
            {type: `separator`},
            {
                label:   `Edit`,
                submenu: [
                    // @formatter:off
                    {label: `Undo`,       role: `undo`,      accelerator: `CmdOrCtrl+Z`},
                    {label: `Redo`,       role: `redo`,      accelerator: `CmdOrCtrl+Y`},
                    {type: `separator`},
                    {label: `Cut`,        role: `cut`,       accelerator: `CmdOrCtrl+X`},
                    {label: `Copy`,       role: `copy`,      accelerator: `CmdOrCtrl+C`},
                    {label: `Paste`,      role: `paste`,     accelerator: `CmdOrCtrl+V`},
                    {label: `Select all`, role: `selectAll`, accelerator: `CmdOrCtrl+A`},
                    // @formatter:on
                    {
                        label:       `Print`,
                        accelerator: `CmdOrCtrl+P`,
                        click:       function (item, focusedWindow?: BrowserWindow): void
                                     {
                                         if (focusedWindow) {
                                             focusedWindow.webContents.print({silent: false, printBackground: true});
                                         }
                                     }
                    },
                ]
            },
            {
                label:   `View`,
                submenu: [
                    {role: `reload`},
                    {role: `reload`, accelerator: `F5`},
                    {
                        type: `separator`
                    },
                    {label: `Zoom in`, role: `zoomIn`, accelerator: `CmdOrCtrl+=`},
                    {label: `Zoom out`, role: `zoomOut`, accelerator: `CmdOrCtrl+-`},
                    {label: `Actual size`, role: `resetZoom`, accelerator: `CmdOrCtrl+0`},
                    {role: `togglefullscreen`},
                ]
            },
            {
                label:   `History`,
                submenu: [
                    {
                        label:       `Forward`,
                        accelerator: `Alt+Right`,
                        click:       function (item, focusedWindow?: BrowserWindow): void
                                     {
                                         if (focusedWindow) {
                                             focusedWindow.webContents.goForward();
                                         }
                                     }
                    },
                    {
                        label:       `Back`,
                        accelerator: `Alt+Left`,
                        click:       function (item, focusedWindow?: BrowserWindow): void
                                     {
                                         if (focusedWindow) {
                                             focusedWindow.webContents.goBack();
                                         }
                                     }
                    }
                ]
            },
            {
                label:   `Window`,
                submenu: [
                    {
                        label: `Minimize`,
                        role:  `minimize`
                    },
                    {
                        label:               `Close`,
                        role:                `close`,
                        accelerator:         MainApp.shortcutStr_showHide,
                        registerAccelerator: false,
                    }
                ]
            },
            MenuBar.settingsMenuItemObj,
        ];
        if (!MainApp.isPackaged) {
            applicationMenuArr.push(
                {
                    visible:     false,
                    label:       `Open dev tools`,
                    accelerator: `F12`,
                    role:        `toggleDevTools`,
                }
            );
            applicationMenuArr.push(
                {
                    visible:     false,
                    label:       `Open dev tools`,
                    accelerator: `Ctrl+Shift+I`,
                    role:        `toggleDevTools`,
                }
            );
        }
        return applicationMenuArr;
    }

}

