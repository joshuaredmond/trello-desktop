import { IpcRendererEvent } from "electron";

const {ipcRenderer, contextBridge} = require(`electron`);

contextBridge.exposeInMainWorld(`electronIpcRenderer`, {
    onMessageReceived: (messageCode: string, listenerFunc: (evt: IpcRendererEvent, ...argsArr: any[]) => void) => ipcRenderer.on(messageCode, listenerFunc), // Allow any.
    sendMessage:       ipcRenderer.send,
});
