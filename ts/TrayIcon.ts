import path = require('path');

import {Menu, Tray} from 'electron';
import {MainWindow} from './MainWindow';
import {MenuBar} from './MenuBar';
import {MainApp} from "./MainApp";

export class TrayIcon
{

    public static trayIconObj: Tray;

    public static init(): void
    {
        TrayIcon.trayIconObj = new Tray(path.join(MainApp.appPath, `assets`, `icon.png`));
        TrayIcon.setTrayMenu();
        TrayIcon.trayIconObj.on(`click`, function (): void
        {
            MainWindow.showHideMainWin(true);
        });
        TrayIcon.trayIconObj.on(`double-click`, function (): void
        {
            MainWindow.showHideMainWin(true);
        });
    }

    public static setTrayMenu(overrideTitleStr?: string, overrideTitleWinId?: number): void
    {
        TrayIcon.trayIconObj.setContextMenu(Menu.buildFromTemplate(MenuBar.getTrayIconMenu(overrideTitleStr, overrideTitleWinId)));
    }
}
