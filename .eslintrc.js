// Allow eslint-disable because it works.
// eslint-disable-next-line no-undef
module.exports = {
    'root':           true,
    'parser':         `@typescript-eslint/parser`,
    'plugins':        [
        `@typescript-eslint`,
    ],
    'extends':        [
        `eslint:recommended`,
        `plugin:@typescript-eslint/recommended`,
    ],
    'env':            {
        'browser': true
    },
    'ignorePatterns': [`generated/js/**/*.js`, `*.json`],
    'rules':          {
        'no-var':                                            0,
        'no-useless-escape':                                 0,
        'prefer-rest-params':                                0, // Because not supported in IE.
        'prefer-spread':                                     0, // Because not supported in IE.
        'prefer-template':                                   1,
        'quotes':                                            `off`,
        '@typescript-eslint/quotes':                         [`error`, `backtick`],
        '@typescript-eslint/ban-ts-comment':                 0,
        '@typescript-eslint/no-var-requires':                0,
        '@typescript-eslint/no-explicit-any':                0,
        '@typescript-eslint/no-inferrable-types':            0,
        '@typescript-eslint/explicit-module-boundary-types': 0,
        '@typescript-eslint/no-namespace':                   0,
        '@typescript-eslint/no-unused-vars':                 `error`,
        '@typescript-eslint/explicit-function-return-type':  `error`
    }
};
