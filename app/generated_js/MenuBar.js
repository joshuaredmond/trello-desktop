"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.MenuBar = void 0;
const electron_1 = require("electron");
const MainApp_1 = require("./MainApp");
const MainWindow_1 = require("./MainWindow");
const SettingsWindow_1 = require("./SettingsWindow");
const path = require("path");
class MenuBar {
    static init() {
        MenuBar.addCardMenuItemObj = {
            label: `Add card`,
            accelerator: MainApp_1.MainApp.shortcutStr_addCard,
            registerAccelerator: false,
            icon: path.join(MainApp_1.MainApp.appPath, `assets`, `menuIcons`, `new.png`),
            click: function () {
                MainWindow_1.MainWindow.showHideMainWin(true);
                MainWindow_1.MainWindow.windowsArr[0].webContents.send(`addCard`, MainApp_1.MainApp.electronStoreObj.get(`defaultListNum`));
            }
        };
        MenuBar.settingsMenuItemObj = {
            label: `Settings`,
            accelerator: `CmdOrCtrl+,`,
            icon: path.join(MainApp_1.MainApp.appPath, `assets`, `menuIcons`, `settings.png`),
            click: function () {
                SettingsWindow_1.SettingsWindow.settingsWin.show();
            }
        };
        MenuBar.copy1stListMenuItemObj = {
            label: `Copy first list`,
            accelerator: MainApp_1.MainApp.shortcutStr_copyDoneList,
            registerAccelerator: false,
            icon: path.join(MainApp_1.MainApp.appPath, `assets`, `menuIcons`, `copy.png`),
            click: function () {
                MainWindow_1.MainWindow.windowsArr[0].webContents.send(`get1stListText`);
            }
        };
        MenuBar.showHideMenuItemObj = {
            label: `Show / hide`,
            accelerator: MainApp_1.MainApp.shortcutStr_showHide,
            registerAccelerator: false,
            click: function () {
                MainWindow_1.MainWindow.showHideMainWin();
            }
        };
        MenuBar.quitMenuItemObj = {
            label: `Quit`,
            click: function () {
                electron_1.app.quit();
            }
        };
        MenuBar.resetAppDataItemObj = {
            label: `Reset app data`,
            click: function () {
                electron_1.dialog.showMessageBox({
                    type: `question`,
                    buttons: [`Yes ... reset`, `Cancel`],
                    detail: `All your preferences are permanently wiped, you'll be logged out, and Trello Desktop restarts.`,
                    defaultId: 0,
                    cancelId: 1,
                    message: `Reset app data?`,
                    icon: MainApp_1.MainApp.appIconPath, // On Linux this icon looks blurry on a high density display. Couldn't fix it.
                }).then(function (responseObj) {
                    if (responseObj.response == 0) {
                        MainApp_1.MainApp.electronStoreObj.clear();
                        electron_1.session.defaultSession.clearCache();
                        electron_1.session.defaultSession.clearStorageData();
                        electron_1.app.relaunch();
                        electron_1.app.exit();
                    }
                });
            }
        };
        MenuBar.reportIssueItemObj = {
            label: `Report issue...`,
            click: function () {
                electron_1.shell.openExternal(`https://gitlab.com/joshuaredmond/trello-desktop/-/issues`);
            }
        };
        electron_1.Menu.setApplicationMenu(electron_1.Menu.buildFromTemplate(MenuBar.getApplicationMenu()));
    }
    static getWindowsMenuItemsArr(overrideTitleStr, overrideTitleWinId) {
        // Add a menu item for each open window.
        var windowsMenuItemsArr = [];
        MainWindow_1.MainWindow.windowsArr.forEach(function (mainWin) {
            var labelStr;
            if (overrideTitleStr && overrideTitleWinId == mainWin.id) { // Pass in `titleStr` because `.getTitle()` hasn't been updated at this point.
                labelStr = overrideTitleStr;
            }
            else {
                labelStr = mainWin.getTitle();
            }
            labelStr = labelStr.replace(` | Trello`, ``);
            labelStr || (labelStr = `Trello`);
            windowsMenuItemsArr.push({
                label: labelStr,
                icon: path.join(MainApp_1.MainApp.appPath, `assets`, `menuIcons`, `logo.png`),
                click: function () {
                    MainWindow_1.MainWindow.moveWindowToStartOfWindowsArr(mainWin);
                    MainWindow_1.MainWindow.setWindowPropertiesArr();
                    MainWindow_1.MainWindow.showHideMainWin(true);
                }
            });
        });
        // Sort alphabetically.
        windowsMenuItemsArr.sort(function (aObj, bObj) {
            return (aObj.label || ``).localeCompare(bObj.label || ``);
        });
        windowsMenuItemsArr.push({
            label: `New window...`,
            icon: path.join(MainApp_1.MainApp.appPath, `assets`, `menuIcons`, `add.png`),
            click: function () {
                MainWindow_1.MainWindow.createWindow({
                    'pageUrl': MainApp_1.MainApp.applicationUrl,
                    'boundsObj': MainWindow_1.MainWindow.getCenteredWindowBounds(MainWindow_1.MainWindow.defaultWidthPx, MainWindow_1.MainWindow.defaultHeightPx),
                });
            }
        });
        return windowsMenuItemsArr;
    }
    static getTrayIconMenu(overrideTitleStr, overrideTitleWinId) {
        return [
            MenuBar.addCardMenuItemObj,
            MenuBar.copy1stListMenuItemObj,
            MenuBar.showHideMenuItemObj,
            { type: `separator` },
            ...MenuBar.getWindowsMenuItemsArr(overrideTitleStr, overrideTitleWinId),
            { type: `separator` },
            MenuBar.settingsMenuItemObj,
            MenuBar.reportIssueItemObj,
            MenuBar.resetAppDataItemObj,
            MenuBar.quitMenuItemObj,
        ];
    }
    static generateRightClickMenu() {
        return [
            ...MenuBar.getWindowsMenuItemsArr(),
            { type: `separator` },
            MenuBar.addCardMenuItemObj,
            MenuBar.copy1stListMenuItemObj,
            MenuBar.showHideMenuItemObj,
            {
                label: `Trello Desktop`,
                submenu: [
                    MenuBar.settingsMenuItemObj,
                    MenuBar.reportIssueItemObj,
                    MenuBar.resetAppDataItemObj,
                ]
            },
        ];
    }
    static getApplicationMenu() {
        var applicationMenuArr = [
            { type: `separator` },
            {
                label: `Edit`,
                submenu: [
                    // @formatter:off
                    { label: `Undo`, role: `undo`, accelerator: `CmdOrCtrl+Z` },
                    { label: `Redo`, role: `redo`, accelerator: `CmdOrCtrl+Y` },
                    { type: `separator` },
                    { label: `Cut`, role: `cut`, accelerator: `CmdOrCtrl+X` },
                    { label: `Copy`, role: `copy`, accelerator: `CmdOrCtrl+C` },
                    { label: `Paste`, role: `paste`, accelerator: `CmdOrCtrl+V` },
                    { label: `Select all`, role: `selectAll`, accelerator: `CmdOrCtrl+A` },
                    // @formatter:on
                    {
                        label: `Print`,
                        accelerator: `CmdOrCtrl+P`,
                        click: function (item, focusedWindow) {
                            if (focusedWindow) {
                                focusedWindow.webContents.print({ silent: false, printBackground: true });
                            }
                        }
                    },
                ]
            },
            {
                label: `View`,
                submenu: [
                    { role: `reload` },
                    { role: `reload`, accelerator: `F5` },
                    {
                        type: `separator`
                    },
                    { label: `Zoom in`, role: `zoomIn`, accelerator: `CmdOrCtrl+=` },
                    { label: `Zoom out`, role: `zoomOut`, accelerator: `CmdOrCtrl+-` },
                    { label: `Actual size`, role: `resetZoom`, accelerator: `CmdOrCtrl+0` },
                    { role: `togglefullscreen` },
                ]
            },
            {
                label: `History`,
                submenu: [
                    {
                        label: `Forward`,
                        accelerator: `Alt+Right`,
                        click: function (item, focusedWindow) {
                            if (focusedWindow) {
                                focusedWindow.webContents.goForward();
                            }
                        }
                    },
                    {
                        label: `Back`,
                        accelerator: `Alt+Left`,
                        click: function (item, focusedWindow) {
                            if (focusedWindow) {
                                focusedWindow.webContents.goBack();
                            }
                        }
                    }
                ]
            },
            {
                label: `Window`,
                submenu: [
                    {
                        label: `Minimize`,
                        role: `minimize`
                    },
                    {
                        label: `Close`,
                        role: `close`,
                        accelerator: MainApp_1.MainApp.shortcutStr_showHide,
                        registerAccelerator: false,
                    }
                ]
            },
            MenuBar.settingsMenuItemObj,
        ];
        if (!MainApp_1.MainApp.isPackaged) {
            applicationMenuArr.push({
                visible: false,
                label: `Open dev tools`,
                accelerator: `F12`,
                role: `toggleDevTools`,
            });
            applicationMenuArr.push({
                visible: false,
                label: `Open dev tools`,
                accelerator: `Ctrl+Shift+I`,
                role: `toggleDevTools`,
            });
        }
        return applicationMenuArr;
    }
}
exports.MenuBar = MenuBar;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiTWVudUJhci5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uL3RzL01lbnVCYXIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7O0FBQUEsdUNBQTBFO0FBQzFFLHVDQUFrQztBQUNsQyw2Q0FBd0M7QUFDeEMscURBQWdEO0FBQ2hELDZCQUE4QjtBQUU5QixNQUFhLE9BQU87SUFXVCxNQUFNLENBQUMsSUFBSTtRQUVkLE9BQU8sQ0FBQyxrQkFBa0IsR0FBTztZQUM3QixLQUFLLEVBQWdCLFVBQVU7WUFDL0IsV0FBVyxFQUFVLGlCQUFPLENBQUMsbUJBQW1CO1lBQ2hELG1CQUFtQixFQUFFLEtBQUs7WUFDMUIsSUFBSSxFQUFpQixJQUFJLENBQUMsSUFBSSxDQUFDLGlCQUFPLENBQUMsT0FBTyxFQUFFLFFBQVEsRUFBRSxXQUFXLEVBQUUsU0FBUyxDQUFDO1lBQ2pGLEtBQUssRUFBZ0I7Z0JBRUksdUJBQVUsQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ2pDLHVCQUFVLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFLGlCQUFPLENBQUMsZ0JBQWdCLENBQUMsR0FBRyxDQUFDLGdCQUFnQixDQUFDLENBQUMsQ0FBQztZQUN6RyxDQUFDO1NBQ3pCLENBQUM7UUFDRixPQUFPLENBQUMsbUJBQW1CLEdBQU07WUFDN0IsS0FBSyxFQUFRLFVBQVU7WUFDdkIsV0FBVyxFQUFFLGFBQWE7WUFDMUIsSUFBSSxFQUFTLElBQUksQ0FBQyxJQUFJLENBQUMsaUJBQU8sQ0FBQyxPQUFPLEVBQUUsUUFBUSxFQUFFLFdBQVcsRUFBRSxjQUFjLENBQUM7WUFDOUUsS0FBSyxFQUFRO2dCQUVJLCtCQUFjLENBQUMsV0FBVyxDQUFDLElBQUksRUFBRSxDQUFDO1lBQ3RDLENBQUM7U0FDakIsQ0FBQztRQUNGLE9BQU8sQ0FBQyxzQkFBc0IsR0FBRztZQUM3QixLQUFLLEVBQWdCLGlCQUFpQjtZQUN0QyxXQUFXLEVBQVUsaUJBQU8sQ0FBQyx3QkFBd0I7WUFDckQsbUJBQW1CLEVBQUUsS0FBSztZQUMxQixJQUFJLEVBQWlCLElBQUksQ0FBQyxJQUFJLENBQUMsaUJBQU8sQ0FBQyxPQUFPLEVBQUUsUUFBUSxFQUFFLFdBQVcsRUFBRSxVQUFVLENBQUM7WUFDbEYsS0FBSyxFQUFnQjtnQkFFSSx1QkFBVSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLENBQUM7WUFDaEUsQ0FBQztTQUN6QixDQUFDO1FBQ0YsT0FBTyxDQUFDLG1CQUFtQixHQUFNO1lBQzdCLEtBQUssRUFBZ0IsYUFBYTtZQUNsQyxXQUFXLEVBQVUsaUJBQU8sQ0FBQyxvQkFBb0I7WUFDakQsbUJBQW1CLEVBQUUsS0FBSztZQUMxQixLQUFLLEVBQWdCO2dCQUVJLHVCQUFVLENBQUMsZUFBZSxFQUFFLENBQUM7WUFDakMsQ0FBQztTQUN6QixDQUFDO1FBQ0YsT0FBTyxDQUFDLGVBQWUsR0FBVTtZQUM3QixLQUFLLEVBQUUsTUFBTTtZQUNiLEtBQUssRUFBRTtnQkFFSSxjQUFHLENBQUMsSUFBSSxFQUFFLENBQUM7WUFDZixDQUFDO1NBQ1gsQ0FBQztRQUNGLE9BQU8sQ0FBQyxtQkFBbUIsR0FBTTtZQUM3QixLQUFLLEVBQUUsZ0JBQWdCO1lBQ3ZCLEtBQUssRUFBRTtnQkFFSSxpQkFBTSxDQUFDLGNBQWMsQ0FDaEI7b0JBQ0ksSUFBSSxFQUFPLFVBQVU7b0JBQ3JCLE9BQU8sRUFBSSxDQUFDLGVBQWUsRUFBRSxRQUFRLENBQUM7b0JBQ3RDLE1BQU0sRUFBSyxnR0FBZ0c7b0JBQzNHLFNBQVMsRUFBRSxDQUFDO29CQUNaLFFBQVEsRUFBRyxDQUFDO29CQUNaLE9BQU8sRUFBSSxpQkFBaUI7b0JBQzVCLElBQUksRUFBTyxpQkFBTyxDQUFDLFdBQVcsRUFBRSw4RUFBOEU7aUJBQ2pILENBQ0osQ0FBQyxJQUFJLENBQ0YsVUFBVSxXQUEyQztvQkFFakQsSUFBSSxXQUFXLENBQUMsUUFBUSxJQUFJLENBQUMsRUFBRSxDQUFDO3dCQUM1QixpQkFBTyxDQUFDLGdCQUFnQixDQUFDLEtBQUssRUFBRSxDQUFDO3dCQUVqQyxrQkFBTyxDQUFDLGNBQWMsQ0FBQyxVQUFVLEVBQUUsQ0FBQzt3QkFDcEMsa0JBQU8sQ0FBQyxjQUFjLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQzt3QkFFMUMsY0FBRyxDQUFDLFFBQVEsRUFBRSxDQUFDO3dCQUNmLGNBQUcsQ0FBQyxJQUFJLEVBQUUsQ0FBQztvQkFDZixDQUFDO2dCQUNMLENBQUMsQ0FDSixDQUFDO1lBQ1AsQ0FBQztTQUNYLENBQUM7UUFDRixPQUFPLENBQUMsa0JBQWtCLEdBQU87WUFDN0IsS0FBSyxFQUFFLGlCQUFpQjtZQUN4QixLQUFLLEVBQUU7Z0JBRUksZ0JBQUssQ0FBQyxZQUFZLENBQUMsMERBQTBELENBQUMsQ0FBQztZQUNuRixDQUFDO1NBQ1gsQ0FBQztRQUNGLGVBQUksQ0FBQyxrQkFBa0IsQ0FBQyxlQUFJLENBQUMsaUJBQWlCLENBQUMsT0FBTyxDQUFDLGtCQUFrQixFQUFFLENBQUMsQ0FBQyxDQUFDO0lBQ2xGLENBQUM7SUFFTyxNQUFNLENBQUMsc0JBQXNCLENBQUMsZ0JBQXlCLEVBQUUsa0JBQTJCO1FBRXhGLHdDQUF3QztRQUN4QyxJQUFJLG1CQUFtQixHQUEwQyxFQUFFLENBQUM7UUFDcEUsdUJBQVUsQ0FBQyxVQUFVLENBQUMsT0FBTyxDQUFDLFVBQVUsT0FBTztZQUUzQyxJQUFJLFFBQVEsQ0FBQztZQUNiLElBQUksZ0JBQWdCLElBQUksa0JBQWtCLElBQUksT0FBTyxDQUFDLEVBQUUsRUFBRSxDQUFDLENBQUMsOEVBQThFO2dCQUN0SSxRQUFRLEdBQUcsZ0JBQWdCLENBQUM7WUFDaEMsQ0FBQztpQkFBTSxDQUFDO2dCQUNKLFFBQVEsR0FBRyxPQUFPLENBQUMsUUFBUSxFQUFFLENBQUM7WUFDbEMsQ0FBQztZQUNELFFBQVEsR0FBRyxRQUFRLENBQUMsT0FBTyxDQUFDLFdBQVcsRUFBRSxFQUFFLENBQUMsQ0FBQztZQUM3QyxRQUFRLEtBQVIsUUFBUSxHQUFLLFFBQVEsRUFBQztZQUN0QixtQkFBbUIsQ0FBQyxJQUFJLENBQ3BCO2dCQUNJLEtBQUssRUFBRSxRQUFRO2dCQUNmLElBQUksRUFBRyxJQUFJLENBQUMsSUFBSSxDQUFDLGlCQUFPLENBQUMsT0FBTyxFQUFFLFFBQVEsRUFBRSxXQUFXLEVBQUUsVUFBVSxDQUFDO2dCQUNwRSxLQUFLLEVBQUU7b0JBRUksdUJBQVUsQ0FBQyw2QkFBNkIsQ0FBQyxPQUFPLENBQUMsQ0FBQztvQkFDbEQsdUJBQVUsQ0FBQyxzQkFBc0IsRUFBRSxDQUFDO29CQUNwQyx1QkFBVSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDckMsQ0FBQzthQUNYLENBQ0osQ0FBQztRQUNOLENBQUMsQ0FBQyxDQUFDO1FBQ0gsdUJBQXVCO1FBQ3ZCLG1CQUFtQixDQUFDLElBQUksQ0FBQyxVQUFVLElBQUksRUFBRSxJQUFJO1lBRXpDLE9BQU8sQ0FBQyxJQUFJLENBQUMsS0FBSyxJQUFJLEVBQUUsQ0FBQyxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsS0FBSyxJQUFJLEVBQUUsQ0FBQyxDQUFDO1FBQzlELENBQUMsQ0FBQyxDQUFDO1FBQ0gsbUJBQW1CLENBQUMsSUFBSSxDQUNwQjtZQUNJLEtBQUssRUFBRSxlQUFlO1lBQ3RCLElBQUksRUFBRyxJQUFJLENBQUMsSUFBSSxDQUFDLGlCQUFPLENBQUMsT0FBTyxFQUFFLFFBQVEsRUFBRSxXQUFXLEVBQUUsU0FBUyxDQUFDO1lBQ25FLEtBQUssRUFBRTtnQkFFSSx1QkFBVSxDQUFDLFlBQVksQ0FBQztvQkFDcEIsU0FBUyxFQUFJLGlCQUFPLENBQUMsY0FBYztvQkFDbkMsV0FBVyxFQUFFLHVCQUFVLENBQUMsdUJBQXVCLENBQUMsdUJBQVUsQ0FBQyxjQUFjLEVBQUUsdUJBQVUsQ0FBQyxlQUFlLENBQUM7aUJBQ3pHLENBQUMsQ0FBQztZQUNQLENBQUM7U0FDWCxDQUNKLENBQUM7UUFDRixPQUFPLG1CQUFtQixDQUFDO0lBQy9CLENBQUM7SUFFTSxNQUFNLENBQUMsZUFBZSxDQUFDLGdCQUF5QixFQUFFLGtCQUEyQjtRQUVoRixPQUFPO1lBQ0gsT0FBTyxDQUFDLGtCQUFrQjtZQUMxQixPQUFPLENBQUMsc0JBQXNCO1lBQzlCLE9BQU8sQ0FBQyxtQkFBbUI7WUFDM0IsRUFBQyxJQUFJLEVBQUUsV0FBVyxFQUFDO1lBQ25CLEdBQUcsT0FBTyxDQUFDLHNCQUFzQixDQUFDLGdCQUFnQixFQUFFLGtCQUFrQixDQUFDO1lBQ3ZFLEVBQUMsSUFBSSxFQUFFLFdBQVcsRUFBQztZQUNuQixPQUFPLENBQUMsbUJBQW1CO1lBQzNCLE9BQU8sQ0FBQyxrQkFBa0I7WUFDMUIsT0FBTyxDQUFDLG1CQUFtQjtZQUMzQixPQUFPLENBQUMsZUFBZTtTQUMxQixDQUFDO0lBQ04sQ0FBQztJQUVNLE1BQU0sQ0FBQyxzQkFBc0I7UUFFaEMsT0FBTztZQUNILEdBQUcsT0FBTyxDQUFDLHNCQUFzQixFQUFFO1lBQ25DLEVBQUMsSUFBSSxFQUFFLFdBQVcsRUFBQztZQUNuQixPQUFPLENBQUMsa0JBQWtCO1lBQzFCLE9BQU8sQ0FBQyxzQkFBc0I7WUFDOUIsT0FBTyxDQUFDLG1CQUFtQjtZQUMzQjtnQkFDSSxLQUFLLEVBQUUsZ0JBQWdCO2dCQUN2QixPQUFPLEVBQ0E7b0JBQ0ksT0FBTyxDQUFDLG1CQUFtQjtvQkFDM0IsT0FBTyxDQUFDLGtCQUFrQjtvQkFDMUIsT0FBTyxDQUFDLG1CQUFtQjtpQkFDOUI7YUFDWDtTQUNKLENBQUM7SUFDTixDQUFDO0lBRU0sTUFBTSxDQUFDLGtCQUFrQjtRQUU1QixJQUFJLGtCQUFrQixHQUEwQztZQUM1RCxFQUFDLElBQUksRUFBRSxXQUFXLEVBQUM7WUFDbkI7Z0JBQ0ksS0FBSyxFQUFJLE1BQU07Z0JBQ2YsT0FBTyxFQUFFO29CQUNMLGlCQUFpQjtvQkFDakIsRUFBQyxLQUFLLEVBQUUsTUFBTSxFQUFRLElBQUksRUFBRSxNQUFNLEVBQU8sV0FBVyxFQUFFLGFBQWEsRUFBQztvQkFDcEUsRUFBQyxLQUFLLEVBQUUsTUFBTSxFQUFRLElBQUksRUFBRSxNQUFNLEVBQU8sV0FBVyxFQUFFLGFBQWEsRUFBQztvQkFDcEUsRUFBQyxJQUFJLEVBQUUsV0FBVyxFQUFDO29CQUNuQixFQUFDLEtBQUssRUFBRSxLQUFLLEVBQVMsSUFBSSxFQUFFLEtBQUssRUFBUSxXQUFXLEVBQUUsYUFBYSxFQUFDO29CQUNwRSxFQUFDLEtBQUssRUFBRSxNQUFNLEVBQVEsSUFBSSxFQUFFLE1BQU0sRUFBTyxXQUFXLEVBQUUsYUFBYSxFQUFDO29CQUNwRSxFQUFDLEtBQUssRUFBRSxPQUFPLEVBQU8sSUFBSSxFQUFFLE9BQU8sRUFBTSxXQUFXLEVBQUUsYUFBYSxFQUFDO29CQUNwRSxFQUFDLEtBQUssRUFBRSxZQUFZLEVBQUUsSUFBSSxFQUFFLFdBQVcsRUFBRSxXQUFXLEVBQUUsYUFBYSxFQUFDO29CQUNwRSxnQkFBZ0I7b0JBQ2hCO3dCQUNJLEtBQUssRUFBUSxPQUFPO3dCQUNwQixXQUFXLEVBQUUsYUFBYTt3QkFDMUIsS0FBSyxFQUFRLFVBQVUsSUFBSSxFQUFFLGFBQTZCOzRCQUV6QyxJQUFJLGFBQWEsRUFBRSxDQUFDO2dDQUNoQixhQUFhLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxFQUFDLE1BQU0sRUFBRSxLQUFLLEVBQUUsZUFBZSxFQUFFLElBQUksRUFBQyxDQUFDLENBQUM7NEJBQzVFLENBQUM7d0JBQ0wsQ0FBQztxQkFDakI7aUJBQ0o7YUFDSjtZQUNEO2dCQUNJLEtBQUssRUFBSSxNQUFNO2dCQUNmLE9BQU8sRUFBRTtvQkFDTCxFQUFDLElBQUksRUFBRSxRQUFRLEVBQUM7b0JBQ2hCLEVBQUMsSUFBSSxFQUFFLFFBQVEsRUFBRSxXQUFXLEVBQUUsSUFBSSxFQUFDO29CQUNuQzt3QkFDSSxJQUFJLEVBQUUsV0FBVztxQkFDcEI7b0JBQ0QsRUFBQyxLQUFLLEVBQUUsU0FBUyxFQUFFLElBQUksRUFBRSxRQUFRLEVBQUUsV0FBVyxFQUFFLGFBQWEsRUFBQztvQkFDOUQsRUFBQyxLQUFLLEVBQUUsVUFBVSxFQUFFLElBQUksRUFBRSxTQUFTLEVBQUUsV0FBVyxFQUFFLGFBQWEsRUFBQztvQkFDaEUsRUFBQyxLQUFLLEVBQUUsYUFBYSxFQUFFLElBQUksRUFBRSxXQUFXLEVBQUUsV0FBVyxFQUFFLGFBQWEsRUFBQztvQkFDckUsRUFBQyxJQUFJLEVBQUUsa0JBQWtCLEVBQUM7aUJBQzdCO2FBQ0o7WUFDRDtnQkFDSSxLQUFLLEVBQUksU0FBUztnQkFDbEIsT0FBTyxFQUFFO29CQUNMO3dCQUNJLEtBQUssRUFBUSxTQUFTO3dCQUN0QixXQUFXLEVBQUUsV0FBVzt3QkFDeEIsS0FBSyxFQUFRLFVBQVUsSUFBSSxFQUFFLGFBQTZCOzRCQUV6QyxJQUFJLGFBQWEsRUFBRSxDQUFDO2dDQUNoQixhQUFhLENBQUMsV0FBVyxDQUFDLFNBQVMsRUFBRSxDQUFDOzRCQUMxQyxDQUFDO3dCQUNMLENBQUM7cUJBQ2pCO29CQUNEO3dCQUNJLEtBQUssRUFBUSxNQUFNO3dCQUNuQixXQUFXLEVBQUUsVUFBVTt3QkFDdkIsS0FBSyxFQUFRLFVBQVUsSUFBSSxFQUFFLGFBQTZCOzRCQUV6QyxJQUFJLGFBQWEsRUFBRSxDQUFDO2dDQUNoQixhQUFhLENBQUMsV0FBVyxDQUFDLE1BQU0sRUFBRSxDQUFDOzRCQUN2QyxDQUFDO3dCQUNMLENBQUM7cUJBQ2pCO2lCQUNKO2FBQ0o7WUFDRDtnQkFDSSxLQUFLLEVBQUksUUFBUTtnQkFDakIsT0FBTyxFQUFFO29CQUNMO3dCQUNJLEtBQUssRUFBRSxVQUFVO3dCQUNqQixJQUFJLEVBQUcsVUFBVTtxQkFDcEI7b0JBQ0Q7d0JBQ0ksS0FBSyxFQUFnQixPQUFPO3dCQUM1QixJQUFJLEVBQWlCLE9BQU87d0JBQzVCLFdBQVcsRUFBVSxpQkFBTyxDQUFDLG9CQUFvQjt3QkFDakQsbUJBQW1CLEVBQUUsS0FBSztxQkFDN0I7aUJBQ0o7YUFDSjtZQUNELE9BQU8sQ0FBQyxtQkFBbUI7U0FDOUIsQ0FBQztRQUNGLElBQUksQ0FBQyxpQkFBTyxDQUFDLFVBQVUsRUFBRSxDQUFDO1lBQ3RCLGtCQUFrQixDQUFDLElBQUksQ0FDbkI7Z0JBQ0ksT0FBTyxFQUFNLEtBQUs7Z0JBQ2xCLEtBQUssRUFBUSxnQkFBZ0I7Z0JBQzdCLFdBQVcsRUFBRSxLQUFLO2dCQUNsQixJQUFJLEVBQVMsZ0JBQWdCO2FBQ2hDLENBQ0osQ0FBQztZQUNGLGtCQUFrQixDQUFDLElBQUksQ0FDbkI7Z0JBQ0ksT0FBTyxFQUFNLEtBQUs7Z0JBQ2xCLEtBQUssRUFBUSxnQkFBZ0I7Z0JBQzdCLFdBQVcsRUFBRSxjQUFjO2dCQUMzQixJQUFJLEVBQVMsZ0JBQWdCO2FBQ2hDLENBQ0osQ0FBQztRQUNOLENBQUM7UUFDRCxPQUFPLGtCQUFrQixDQUFDO0lBQzlCLENBQUM7Q0FFSjtBQWhTRCwwQkFnU0MifQ==