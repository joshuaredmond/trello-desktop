"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SettingsWindow = void 0;
const path_1 = __importDefault(require("path"));
const electron_1 = require("electron");
const MainApp_1 = require("./MainApp");
const MainWindow_1 = require("./MainWindow");
class SettingsWindow {
    static init() {
        var isDebug = false;
        var widthPx = (isDebug) ? 1500 : 338;
        var heightPx = (isDebug) ? 1000 : 270;
        var boundsObj = MainWindow_1.MainWindow.getCenteredWindowBounds(widthPx, heightPx);
        SettingsWindow.settingsWin = new electron_1.BrowserWindow({
            width: boundsObj.width,
            height: boundsObj.height,
            x: boundsObj.x,
            y: boundsObj.y,
            resizable: isDebug,
            show: false,
            icon: MainApp_1.MainApp.appIconPath,
            title: `Settings`,
            webPreferences: {
                devTools: isDebug,
                preload: path_1.default.join(__dirname, `SettingsWindowPreload.js`),
            },
        });
        SettingsWindow.settingsWin.removeMenu();
        SettingsWindow.settingsWin.on(`ready-to-show`, function () {
            SettingsWindow.settingsWin.webContents.send(`settingsObj`, MainApp_1.MainApp.electronStoreObj.get(`defaultListNum`), MainApp_1.MainApp.electronStoreObj.get(`isRunAtStartup`), MainApp_1.MainApp.electronStoreObj.get(`isAddEnhancements`), MainApp_1.MainApp.packageJsonObj.version);
        });
        if (isDebug) {
            SettingsWindow.settingsWin.on(`resize`, function () {
                console.log(SettingsWindow.settingsWin.getSize()); // Allow `console.log`.
            });
        }
        SettingsWindow.settingsWin.on(`show`, function () {
            SettingsWindow.settingsWin.setBounds(MainWindow_1.MainWindow.getCenteredWindowBounds(widthPx, heightPx));
            SettingsWindow.settingsWin.setAlwaysOnTop(true); // Using `alwaysOnTop` doesn't work, calling this function outside `show` doesn't work either.
        });
        SettingsWindow.settingsWin.on(`close`, function (evt) {
            if (!MainApp_1.MainApp.isQuitting) {
                evt.preventDefault();
                SettingsWindow.settingsWin.hide();
            }
        });
        SettingsWindow.settingsWin.webContents.on(`will-navigate`, function (evt) {
            evt.preventDefault();
        });
        SettingsWindow.settingsWin.loadURL(`file://${__dirname}/../settingsWindow.html`);
    }
}
exports.SettingsWindow = SettingsWindow;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiU2V0dGluZ3NXaW5kb3cuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi90cy9TZXR0aW5nc1dpbmRvdy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7QUFBQSxnREFBd0I7QUFDeEIsdUNBQXVDO0FBQ3ZDLHVDQUFrQztBQUNsQyw2Q0FBd0M7QUFFeEMsTUFBYSxjQUFjO0lBS2hCLE1BQU0sQ0FBQyxJQUFJO1FBRWQsSUFBSSxPQUFPLEdBQWtCLEtBQUssQ0FBQztRQUNuQyxJQUFJLE9BQU8sR0FBa0IsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUM7UUFDcEQsSUFBSSxRQUFRLEdBQWlCLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDO1FBQ3BELElBQUksU0FBUyxHQUFnQix1QkFBVSxDQUFDLHVCQUF1QixDQUFDLE9BQU8sRUFBRSxRQUFRLENBQUMsQ0FBQztRQUNuRixjQUFjLENBQUMsV0FBVyxHQUFHLElBQUksd0JBQWEsQ0FBQztZQUMzQyxLQUFLLEVBQVcsU0FBUyxDQUFDLEtBQUs7WUFDL0IsTUFBTSxFQUFVLFNBQVMsQ0FBQyxNQUFNO1lBQ2hDLENBQUMsRUFBZSxTQUFTLENBQUMsQ0FBQztZQUMzQixDQUFDLEVBQWUsU0FBUyxDQUFDLENBQUM7WUFDM0IsU0FBUyxFQUFPLE9BQU87WUFDdkIsSUFBSSxFQUFZLEtBQUs7WUFDckIsSUFBSSxFQUFZLGlCQUFPLENBQUMsV0FBVztZQUNuQyxLQUFLLEVBQVcsVUFBVTtZQUMxQixjQUFjLEVBQUU7Z0JBQ1osUUFBUSxFQUFFLE9BQU87Z0JBQ2pCLE9BQU8sRUFBRyxjQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFBRSwwQkFBMEIsQ0FBQzthQUM3RDtTQUNKLENBQUMsQ0FBQztRQUNILGNBQWMsQ0FBQyxXQUFXLENBQUMsVUFBVSxFQUFFLENBQUM7UUFDeEMsY0FBYyxDQUFDLFdBQVcsQ0FBQyxFQUFFLENBQUMsZUFBZSxFQUFFO1lBRTNDLGNBQWMsQ0FBQyxXQUFXLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxhQUFhLEVBQUUsaUJBQU8sQ0FBQyxnQkFBZ0IsQ0FBQyxHQUFHLENBQUMsZ0JBQWdCLENBQUMsRUFBRSxpQkFBTyxDQUFDLGdCQUFnQixDQUFDLEdBQUcsQ0FBQyxnQkFBZ0IsQ0FBQyxFQUFFLGlCQUFPLENBQUMsZ0JBQWdCLENBQUMsR0FBRyxDQUFDLG1CQUFtQixDQUFDLEVBQUUsaUJBQU8sQ0FBQyxjQUFjLENBQUMsT0FBTyxDQUFDLENBQUM7UUFDbFAsQ0FBQyxDQUFDLENBQUM7UUFDSCxJQUFJLE9BQU8sRUFBRSxDQUFDO1lBQ1YsY0FBYyxDQUFDLFdBQVcsQ0FBQyxFQUFFLENBQUMsUUFBUSxFQUFFO2dCQUVwQyxPQUFPLENBQUMsR0FBRyxDQUFDLGNBQWMsQ0FBQyxXQUFXLENBQUMsT0FBTyxFQUFFLENBQUMsQ0FBQyxDQUFDLHVCQUF1QjtZQUM5RSxDQUFDLENBQUMsQ0FBQztRQUNQLENBQUM7UUFDRCxjQUFjLENBQUMsV0FBVyxDQUFDLEVBQUUsQ0FBQyxNQUFNLEVBQUU7WUFFbEMsY0FBYyxDQUFDLFdBQVcsQ0FBQyxTQUFTLENBQUMsdUJBQVUsQ0FBQyx1QkFBdUIsQ0FBQyxPQUFPLEVBQUUsUUFBUSxDQUFDLENBQUMsQ0FBQztZQUM1RixjQUFjLENBQUMsV0FBVyxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLDhGQUE4RjtRQUNuSixDQUFDLENBQUMsQ0FBQztRQUNILGNBQWMsQ0FBQyxXQUFXLENBQUMsRUFBRSxDQUFDLE9BQU8sRUFBRSxVQUFVLEdBQUc7WUFFaEQsSUFBSSxDQUFDLGlCQUFPLENBQUMsVUFBVSxFQUFFLENBQUM7Z0JBQ3RCLEdBQUcsQ0FBQyxjQUFjLEVBQUUsQ0FBQztnQkFDckIsY0FBYyxDQUFDLFdBQVcsQ0FBQyxJQUFJLEVBQUUsQ0FBQztZQUN0QyxDQUFDO1FBQ0wsQ0FBQyxDQUFDLENBQUM7UUFDSCxjQUFjLENBQUMsV0FBVyxDQUFDLFdBQVcsQ0FBQyxFQUFFLENBQUMsZUFBZSxFQUFFLFVBQVUsR0FBRztZQUVwRSxHQUFHLENBQUMsY0FBYyxFQUFFLENBQUM7UUFDekIsQ0FBQyxDQUFDLENBQUM7UUFDSCxjQUFjLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQyxVQUFVLFNBQVMseUJBQXlCLENBQUMsQ0FBQztJQUNyRixDQUFDO0NBRUo7QUF2REQsd0NBdURDIn0=