"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const { ipcRenderer, contextBridge } = require(`electron`);
contextBridge.exposeInMainWorld(`electronIpcRenderer`, {
    onMessageReceived: (messageCode, listenerFunc) => ipcRenderer.on(messageCode, listenerFunc), // Allow any.
    sendMessage: ipcRenderer.send,
});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiU2V0dGluZ3NXaW5kb3dQcmVsb2FkLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiLi4vLi4vdHMvU2V0dGluZ3NXaW5kb3dQcmVsb2FkLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBRUEsTUFBTSxFQUFDLFdBQVcsRUFBRSxhQUFhLEVBQUMsR0FBRyxPQUFPLENBQUMsVUFBVSxDQUFDLENBQUM7QUFFekQsYUFBYSxDQUFDLGlCQUFpQixDQUFDLHFCQUFxQixFQUFFO0lBQ25ELGlCQUFpQixFQUFFLENBQUMsV0FBbUIsRUFBRSxZQUFnRSxFQUFFLEVBQUUsQ0FBQyxXQUFXLENBQUMsRUFBRSxDQUFDLFdBQVcsRUFBRSxZQUFZLENBQUMsRUFBRSxhQUFhO0lBQ3RLLFdBQVcsRUFBUSxXQUFXLENBQUMsSUFBSTtDQUN0QyxDQUFDLENBQUMifQ==