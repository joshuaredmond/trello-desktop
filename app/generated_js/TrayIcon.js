"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TrayIcon = void 0;
const path = require("path");
const electron_1 = require("electron");
const MainWindow_1 = require("./MainWindow");
const MenuBar_1 = require("./MenuBar");
const MainApp_1 = require("./MainApp");
class TrayIcon {
    static init() {
        TrayIcon.trayIconObj = new electron_1.Tray(path.join(MainApp_1.MainApp.appPath, `assets`, `icon.png`));
        TrayIcon.setTrayMenu();
        TrayIcon.trayIconObj.on(`click`, function () {
            MainWindow_1.MainWindow.showHideMainWin(true);
        });
        TrayIcon.trayIconObj.on(`double-click`, function () {
            MainWindow_1.MainWindow.showHideMainWin(true);
        });
    }
    static setTrayMenu(overrideTitleStr, overrideTitleWinId) {
        TrayIcon.trayIconObj.setContextMenu(electron_1.Menu.buildFromTemplate(MenuBar_1.MenuBar.getTrayIconMenu(overrideTitleStr, overrideTitleWinId)));
    }
}
exports.TrayIcon = TrayIcon;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiVHJheUljb24uanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi90cy9UcmF5SWNvbi50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7QUFBQSw2QkFBOEI7QUFFOUIsdUNBQW9DO0FBQ3BDLDZDQUF3QztBQUN4Qyx1Q0FBa0M7QUFDbEMsdUNBQWtDO0FBRWxDLE1BQWEsUUFBUTtJQUtWLE1BQU0sQ0FBQyxJQUFJO1FBRWQsUUFBUSxDQUFDLFdBQVcsR0FBRyxJQUFJLGVBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGlCQUFPLENBQUMsT0FBTyxFQUFFLFFBQVEsRUFBRSxVQUFVLENBQUMsQ0FBQyxDQUFDO1FBQ2xGLFFBQVEsQ0FBQyxXQUFXLEVBQUUsQ0FBQztRQUN2QixRQUFRLENBQUMsV0FBVyxDQUFDLEVBQUUsQ0FBQyxPQUFPLEVBQUU7WUFFN0IsdUJBQVUsQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDckMsQ0FBQyxDQUFDLENBQUM7UUFDSCxRQUFRLENBQUMsV0FBVyxDQUFDLEVBQUUsQ0FBQyxjQUFjLEVBQUU7WUFFcEMsdUJBQVUsQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDckMsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRU0sTUFBTSxDQUFDLFdBQVcsQ0FBQyxnQkFBeUIsRUFBRSxrQkFBMkI7UUFFNUUsUUFBUSxDQUFDLFdBQVcsQ0FBQyxjQUFjLENBQUMsZUFBSSxDQUFDLGlCQUFpQixDQUFDLGlCQUFPLENBQUMsZUFBZSxDQUFDLGdCQUFnQixFQUFFLGtCQUFrQixDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQy9ILENBQUM7Q0FDSjtBQXZCRCw0QkF1QkMifQ==