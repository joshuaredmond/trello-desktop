"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MainWindow = void 0;
const electron_1 = require("electron");
const MainApp_1 = require("./MainApp");
const MenuBar_1 = require("./MenuBar");
const TrayIcon_1 = require("./TrayIcon");
const path = require("path");
class MainWindow {
    static init() {
        var windowPropertiesArr = MainApp_1.MainApp.electronStoreObj.get(`windowPropertiesArr`);
        if (windowPropertiesArr.length) {
            windowPropertiesArr.forEach(function (windowPropertiesObj) {
                MainWindow.createWindow(windowPropertiesObj);
            });
        }
        else {
            MainWindow.createWindow({
                'pageUrl': MainApp_1.MainApp.applicationUrl,
                'boundsObj': MainWindow.getCenteredWindowBounds(MainWindow.defaultWidthPx, MainWindow.defaultHeightPx),
            });
        }
    }
    static createWindow(windowPropertiesObj) {
        var mainWin = new electron_1.BrowserWindow({
            title: MainApp_1.MainApp.packageJsonObj.productName,
            x: windowPropertiesObj.boundsObj.x,
            y: windowPropertiesObj.boundsObj.y,
            width: windowPropertiesObj.boundsObj.width,
            height: windowPropertiesObj.boundsObj.height,
            icon: MainApp_1.MainApp.appIconPath,
            webPreferences: {
                preload: path.join(__dirname, `MainWindowPreload.js`),
                devTools: !MainApp_1.MainApp.isPackaged,
                nodeIntegration: true,
            }
        });
        mainWin.setMenuBarVisibility(false);
        if (windowPropertiesObj.pageUrl && windowPropertiesObj.pageUrl.startsWith(MainApp_1.MainApp.applicationUrl)) {
            mainWin.loadURL(windowPropertiesObj.pageUrl);
        }
        else {
            mainWin.loadURL(MainApp_1.MainApp.applicationUrl);
        }
        MainWindow.setupWindowEvents(mainWin);
        MainWindow.windowsArr.push(mainWin);
    }
    static setupWindowEvents(mainWin) {
        mainWin.on(`close`, function (evt) {
            if (!MainApp_1.MainApp.isQuitting) {
                if (MainWindow.windowsArr.length > 1) {
                    MainWindow.windowsArr.splice(MainWindow.windowsArr.indexOf(mainWin), 1); // Remove from `MainWindow.windowsArr`.
                    MainWindow.setWindowPropertiesArr();
                    TrayIcon_1.TrayIcon.setTrayMenu();
                }
                else { // Don't allow closing of last window.
                    evt.preventDefault();
                    mainWin.hide();
                }
            }
        });
        mainWin.on(`focus`, function () {
            if (!MainWindow.isFreezingWindowOrder) {
                // Move window to beginnining of `MainWindow.windowsArr`.
                MainWindow.moveWindowToStartOfWindowsArr(mainWin);
                MainWindow.setWindowPropertiesArr();
            }
        });
        mainWin.on(`leave-full-screen`, function () {
            mainWin.setMenuBarVisibility(false);
        });
        // @formatter:off
        mainWin.on(`page-title-updated`, function (evt, titleStr) { TrayIcon_1.TrayIcon.setTrayMenu(titleStr, mainWin.id); }); // Pass in `titleStr` because `.getTitle()` hasn't been updated at this point.
        mainWin.on(`move`, function () { MainWindow.setWindowPropertiesArr(); });
        mainWin.on(`resize`, function () { MainWindow.setWindowPropertiesArr(); });
        mainWin.webContents.on(`did-navigate`, function () { MainWindow.setWindowPropertiesArr(); TrayIcon_1.TrayIcon.setTrayMenu(); });
        mainWin.webContents.on(`did-navigate-in-page`, function () { MainWindow.setWindowPropertiesArr(); TrayIcon_1.TrayIcon.setTrayMenu(); });
        // @formatter:on
        mainWin.webContents.on(`context-menu`, function () {
            // On Mac (darwin) it doesn`t focus the window if you right-click on it.
            if (MainApp_1.MainApp.platformType == `mac`) {
                mainWin.focus();
            }
            electron_1.Menu.buildFromTemplate(MenuBar_1.MenuBar.generateRightClickMenu()).popup({});
        });
        mainWin.webContents.setWindowOpenHandler(function (detailsObj) {
            if (detailsObj.url.startsWith(MainApp_1.MainApp.applicationUrl)) {
                MainWindow.createWindow({
                    'pageUrl': detailsObj.url,
                    'boundsObj': MainWindow.getCenteredWindowBounds(MainWindow.defaultWidthPx, MainWindow.defaultHeightPx),
                });
            }
            else {
                electron_1.shell.openExternal(detailsObj.url);
            }
            return { action: `deny` };
        });
        mainWin.webContents.on(`will-navigate`, function (evt, navigateUrl) {
            if (!MainApp_1.MainApp.urlContainsAllowlistArr.some((urlContainsStr) => navigateUrl.includes(urlContainsStr))) {
                electron_1.shell.openExternal(navigateUrl);
                evt.preventDefault();
            }
        });
        mainWin.webContents.ipc.handle(`isAddEnhancements`, function () {
            return __awaiter(this, void 0, void 0, function* () {
                return MainApp_1.MainApp.electronStoreObj.get(`isAddEnhancements`);
            });
        });
    }
    static moveWindowToStartOfWindowsArr(mainWin) {
        var winIndexNum = MainWindow.windowsArr.indexOf(mainWin);
        if (winIndexNum != 0) {
            MainWindow.windowsArr.splice(winIndexNum, 1);
            MainWindow.windowsArr.unshift(mainWin);
        }
    }
    static getCenteredWindowBounds(widthPx, heightPx) {
        var mainScreenBoundsObj = electron_1.screen.getDisplayNearestPoint(electron_1.screen.getCursorScreenPoint()).bounds;
        var winX = mainScreenBoundsObj.x + ((mainScreenBoundsObj.width - widthPx) / 2);
        var winY = mainScreenBoundsObj.y + ((mainScreenBoundsObj.height - heightPx) / 2);
        return {
            'width': widthPx,
            'height': heightPx,
            'x': Math.round(winX), // Math.round needed since it throws a strange error without it.
            'y': Math.round(winY) // Math.round needed since it throws a strange error without it.
        };
    }
    static runOnAllWindows(runFunc) {
        for (var i = MainWindow.windowsArr.length - 1; i >= 0; i--) {
            if (!MainWindow.windowsArr[i].isDestroyed()) {
                runFunc(i, MainWindow.windowsArr[i]);
            }
        }
    }
    /*
     * Save all the window's state, for app re-opening.
     * Can't call in `before-quit` because `before-quit` isn't triggered from a Ctrl+C in the terminal.
     */
    static setWindowPropertiesArr() {
        var windowPropertiesArr = [];
        MainWindow.runOnAllWindows(function (indexNum, mainWin) {
            windowPropertiesArr.push({
                'pageUrl': mainWin.webContents.getURL(),
                'boundsObj': mainWin.getBounds(),
            });
        });
        MainApp_1.MainApp.electronStoreObj.set(`windowPropertiesArr`, windowPropertiesArr);
    }
    static showHideMainWin(isShow) {
        if (( // If `isShow` is passed in, use that instead.
        isShow !== undefined
            &&
                isShow)
            ||
                (isShow === undefined
                    &&
                        (!MainWindow.windowsArr[0].isVisible()
                            ||
                                !MainWindow.windowsArr[0].isFocused()))) {
            MainWindow.isFreezingWindowOrder = true;
            MainWindow.runOnAllWindows(function (indexNum, mainWin) {
                // Show will focus the window, sometimes (not reliable).
                // The `focus` event is fired asynchronously, and sometimes in a different order (not reliable).
                mainWin.show(); // Can't use `showInactive` here because you'd get "app is ready" notification on Ubuntu 20.04.
            });
            MainWindow.windowsArr[0].focus();
            // Using a `setTimeout` is the only way I've found to reliably show and hide all the windows in the correct order.
            clearTimeout(MainWindow.unfreezeWindowOrderTimeout);
            MainWindow.unfreezeWindowOrderTimeout = setTimeout(function () {
                MainWindow.isFreezingWindowOrder = false;
            }, 400);
        }
        else {
            MainWindow.runOnAllWindows(function (indexNum, mainWin) {
                mainWin.hide();
            });
        }
    }
}
exports.MainWindow = MainWindow;
MainWindow.windowsArr = []; // Sorted by last focus, focusing puts the window at the beginning. E.g., `MainWindow.windowsArr[0]` gets the last focused window.
MainWindow.defaultWidthPx = 1460;
MainWindow.defaultHeightPx = 800;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiTWFpbldpbmRvdy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uL3RzL01haW5XaW5kb3cudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7O0FBQUEsdUNBQTREO0FBQzVELHVDQUFrQztBQUNsQyx1Q0FBa0M7QUFDbEMseUNBQW9DO0FBQ3BDLDZCQUE4QjtBQUU5QixNQUFhLFVBQVU7SUFTWixNQUFNLENBQUMsSUFBSTtRQUVkLElBQUksbUJBQW1CLEdBQUcsaUJBQU8sQ0FBQyxnQkFBZ0IsQ0FBQyxHQUFHLENBQUMscUJBQXFCLENBQUMsQ0FBQztRQUM5RSxJQUFJLG1CQUFtQixDQUFDLE1BQU0sRUFBRSxDQUFDO1lBQzdCLG1CQUFtQixDQUFDLE9BQU8sQ0FBQyxVQUFVLG1CQUFtQjtnQkFFckQsVUFBVSxDQUFDLFlBQVksQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDO1lBQ2pELENBQUMsQ0FBQyxDQUFDO1FBQ1AsQ0FBQzthQUFNLENBQUM7WUFDSixVQUFVLENBQUMsWUFBWSxDQUFDO2dCQUNwQixTQUFTLEVBQUksaUJBQU8sQ0FBQyxjQUFjO2dCQUNuQyxXQUFXLEVBQUUsVUFBVSxDQUFDLHVCQUF1QixDQUFDLFVBQVUsQ0FBQyxjQUFjLEVBQUUsVUFBVSxDQUFDLGVBQWUsQ0FBQzthQUN6RyxDQUFDLENBQUM7UUFDUCxDQUFDO0lBQ0wsQ0FBQztJQUVNLE1BQU0sQ0FBQyxZQUFZLENBQUMsbUJBQXFDO1FBRTVELElBQUksT0FBTyxHQUFHLElBQUksd0JBQWEsQ0FBQztZQUM1QixLQUFLLEVBQVcsaUJBQU8sQ0FBQyxjQUFjLENBQUMsV0FBVztZQUNsRCxDQUFDLEVBQWUsbUJBQW1CLENBQUMsU0FBUyxDQUFDLENBQUM7WUFDL0MsQ0FBQyxFQUFlLG1CQUFtQixDQUFDLFNBQVMsQ0FBQyxDQUFDO1lBQy9DLEtBQUssRUFBVyxtQkFBbUIsQ0FBQyxTQUFTLENBQUMsS0FBSztZQUNuRCxNQUFNLEVBQVUsbUJBQW1CLENBQUMsU0FBUyxDQUFDLE1BQU07WUFDcEQsSUFBSSxFQUFZLGlCQUFPLENBQUMsV0FBVztZQUNuQyxjQUFjLEVBQUU7Z0JBQ1osT0FBTyxFQUFVLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFLHNCQUFzQixDQUFDO2dCQUM3RCxRQUFRLEVBQVMsQ0FBQyxpQkFBTyxDQUFDLFVBQVU7Z0JBQ3BDLGVBQWUsRUFBRSxJQUFJO2FBQ3hCO1NBQ0osQ0FBQyxDQUFDO1FBQ0gsT0FBTyxDQUFDLG9CQUFvQixDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQ3BDLElBQUksbUJBQW1CLENBQUMsT0FBTyxJQUFJLG1CQUFtQixDQUFDLE9BQU8sQ0FBQyxVQUFVLENBQUMsaUJBQU8sQ0FBQyxjQUFjLENBQUMsRUFBRSxDQUFDO1lBQ2hHLE9BQU8sQ0FBQyxPQUFPLENBQUMsbUJBQW1CLENBQUMsT0FBTyxDQUFDLENBQUM7UUFDakQsQ0FBQzthQUFNLENBQUM7WUFDSixPQUFPLENBQUMsT0FBTyxDQUFDLGlCQUFPLENBQUMsY0FBYyxDQUFDLENBQUM7UUFDNUMsQ0FBQztRQUNELFVBQVUsQ0FBQyxpQkFBaUIsQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUN0QyxVQUFVLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztJQUN4QyxDQUFDO0lBRU8sTUFBTSxDQUFDLGlCQUFpQixDQUFDLE9BQXNCO1FBRW5ELE9BQU8sQ0FBQyxFQUFFLENBQUMsT0FBTyxFQUFFLFVBQVUsR0FBRztZQUU3QixJQUFJLENBQUMsaUJBQU8sQ0FBQyxVQUFVLEVBQUUsQ0FBQztnQkFDdEIsSUFBSSxVQUFVLENBQUMsVUFBVSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUUsQ0FBQztvQkFDbkMsVUFBVSxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsVUFBVSxDQUFDLFVBQVUsQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyx1Q0FBdUM7b0JBQ2hILFVBQVUsQ0FBQyxzQkFBc0IsRUFBRSxDQUFDO29CQUNwQyxtQkFBUSxDQUFDLFdBQVcsRUFBRSxDQUFDO2dCQUMzQixDQUFDO3FCQUFNLENBQUMsQ0FBQyxzQ0FBc0M7b0JBQzNDLEdBQUcsQ0FBQyxjQUFjLEVBQUUsQ0FBQztvQkFDckIsT0FBTyxDQUFDLElBQUksRUFBRSxDQUFDO2dCQUNuQixDQUFDO1lBQ0wsQ0FBQztRQUNMLENBQUMsQ0FBQyxDQUFDO1FBQ0gsT0FBTyxDQUFDLEVBQUUsQ0FBQyxPQUFPLEVBQUU7WUFFaEIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxxQkFBcUIsRUFBRSxDQUFDO2dCQUNwQyx5REFBeUQ7Z0JBQ3pELFVBQVUsQ0FBQyw2QkFBNkIsQ0FBQyxPQUFPLENBQUMsQ0FBQztnQkFDbEQsVUFBVSxDQUFDLHNCQUFzQixFQUFFLENBQUM7WUFDeEMsQ0FBQztRQUNMLENBQUMsQ0FBQyxDQUFDO1FBQ0gsT0FBTyxDQUFDLEVBQUUsQ0FBQyxtQkFBbUIsRUFBRTtZQUU1QixPQUFPLENBQUMsb0JBQW9CLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDeEMsQ0FBQyxDQUFDLENBQUM7UUFDSCxpQkFBaUI7UUFDakIsT0FBTyxDQUFDLEVBQUUsQ0FBQyxvQkFBb0IsRUFBZ0IsVUFBVSxHQUFHLEVBQUUsUUFBUSxJQUF1QixtQkFBUSxDQUFDLFdBQVcsQ0FBQyxRQUFRLEVBQUUsT0FBTyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQWtCLENBQUMsQ0FBQyxDQUFDLENBQUMsOEVBQThFO1FBQzVPLE9BQU8sQ0FBQyxFQUFFLENBQUMsTUFBTSxFQUE4QixjQUE4QyxVQUFVLENBQUMsc0JBQXNCLEVBQUUsQ0FBQyxDQUF5QixDQUFDLENBQUMsQ0FBQztRQUM3SixPQUFPLENBQUMsRUFBRSxDQUFDLFFBQVEsRUFBNEIsY0FBOEMsVUFBVSxDQUFDLHNCQUFzQixFQUFFLENBQUMsQ0FBeUIsQ0FBQyxDQUFDLENBQUM7UUFDN0osT0FBTyxDQUFDLFdBQVcsQ0FBQyxFQUFFLENBQUMsY0FBYyxFQUFVLGNBQThDLFVBQVUsQ0FBQyxzQkFBc0IsRUFBRSxDQUFDLENBQUMsbUJBQVEsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQzdKLE9BQU8sQ0FBQyxXQUFXLENBQUMsRUFBRSxDQUFDLHNCQUFzQixFQUFFLGNBQThDLFVBQVUsQ0FBQyxzQkFBc0IsRUFBRSxDQUFDLENBQUMsbUJBQVEsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQzdKLGdCQUFnQjtRQUNoQixPQUFPLENBQUMsV0FBVyxDQUFDLEVBQUUsQ0FBQyxjQUFjLEVBQUU7WUFFbkMsd0VBQXdFO1lBQ3hFLElBQUksaUJBQU8sQ0FBQyxZQUFZLElBQUksS0FBSyxFQUFFLENBQUM7Z0JBQ2hDLE9BQU8sQ0FBQyxLQUFLLEVBQUUsQ0FBQztZQUNwQixDQUFDO1lBQ0QsZUFBSSxDQUFDLGlCQUFpQixDQUFDLGlCQUFPLENBQUMsc0JBQXNCLEVBQUUsQ0FBQyxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQUMsQ0FBQztRQUN2RSxDQUFDLENBQUMsQ0FBQztRQUNILE9BQU8sQ0FBQyxXQUFXLENBQUMsb0JBQW9CLENBQUMsVUFBVSxVQUFVO1lBRXpELElBQUksVUFBVSxDQUFDLEdBQUcsQ0FBQyxVQUFVLENBQUMsaUJBQU8sQ0FBQyxjQUFjLENBQUMsRUFBRSxDQUFDO2dCQUNwRCxVQUFVLENBQUMsWUFBWSxDQUFDO29CQUNwQixTQUFTLEVBQUksVUFBVSxDQUFDLEdBQUc7b0JBQzNCLFdBQVcsRUFBRSxVQUFVLENBQUMsdUJBQXVCLENBQUMsVUFBVSxDQUFDLGNBQWMsRUFBRSxVQUFVLENBQUMsZUFBZSxDQUFDO2lCQUN6RyxDQUFDLENBQUM7WUFDUCxDQUFDO2lCQUFNLENBQUM7Z0JBQ0osZ0JBQUssQ0FBQyxZQUFZLENBQUMsVUFBVSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQ3ZDLENBQUM7WUFDRCxPQUFPLEVBQUMsTUFBTSxFQUFFLE1BQU0sRUFBQyxDQUFDO1FBQzVCLENBQUMsQ0FBQyxDQUFDO1FBQ0gsT0FBTyxDQUFDLFdBQVcsQ0FBQyxFQUFFLENBQUMsZUFBZSxFQUFFLFVBQVUsR0FBRyxFQUFFLFdBQVc7WUFFOUQsSUFBSSxDQUFDLGlCQUFPLENBQUMsdUJBQXVCLENBQUMsSUFBSSxDQUFDLENBQUMsY0FBYyxFQUFXLEVBQUUsQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLGNBQWMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztnQkFDM0csZ0JBQUssQ0FBQyxZQUFZLENBQUMsV0FBVyxDQUFDLENBQUM7Z0JBQ2hDLEdBQUcsQ0FBQyxjQUFjLEVBQUUsQ0FBQztZQUN6QixDQUFDO1FBQ0wsQ0FBQyxDQUFDLENBQUM7UUFDSCxPQUFPLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsbUJBQW1CLEVBQUU7O2dCQUVoRCxPQUFPLGlCQUFPLENBQUMsZ0JBQWdCLENBQUMsR0FBRyxDQUFDLG1CQUFtQixDQUFDLENBQUM7WUFDN0QsQ0FBQztTQUFBLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFTSxNQUFNLENBQUMsNkJBQTZCLENBQUMsT0FBc0I7UUFFOUQsSUFBSSxXQUFXLEdBQUcsVUFBVSxDQUFDLFVBQVUsQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLENBQUM7UUFDekQsSUFBSSxXQUFXLElBQUksQ0FBQyxFQUFFLENBQUM7WUFDbkIsVUFBVSxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsV0FBVyxFQUFFLENBQUMsQ0FBQyxDQUFDO1lBQzdDLFVBQVUsQ0FBQyxVQUFVLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQzNDLENBQUM7SUFDTCxDQUFDO0lBRU0sTUFBTSxDQUFDLHVCQUF1QixDQUFDLE9BQWUsRUFBRSxRQUFnQjtRQUVuRSxJQUFJLG1CQUFtQixHQUFHLGlCQUFNLENBQUMsc0JBQXNCLENBQUMsaUJBQU0sQ0FBQyxvQkFBb0IsRUFBRSxDQUFDLENBQUMsTUFBTSxDQUFDO1FBQzlGLElBQUksSUFBSSxHQUFrQixtQkFBbUIsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLG1CQUFtQixDQUFDLEtBQUssR0FBRyxPQUFPLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztRQUM5RixJQUFJLElBQUksR0FBa0IsbUJBQW1CLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxtQkFBbUIsQ0FBQyxNQUFNLEdBQUcsUUFBUSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7UUFDaEcsT0FBTztZQUNILE9BQU8sRUFBRyxPQUFPO1lBQ2pCLFFBQVEsRUFBRSxRQUFRO1lBQ2xCLEdBQUcsRUFBTyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxFQUFFLGdFQUFnRTtZQUM1RixHQUFHLEVBQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQyxnRUFBZ0U7U0FDOUYsQ0FBQztJQUNOLENBQUM7SUFFTSxNQUFNLENBQUMsZUFBZSxDQUFDLE9BQTJEO1FBRXJGLEtBQUssSUFBSSxDQUFDLEdBQUcsVUFBVSxDQUFDLFVBQVUsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxFQUFFLEVBQUUsQ0FBQztZQUN6RCxJQUFJLENBQUMsVUFBVSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxXQUFXLEVBQUUsRUFBRSxDQUFDO2dCQUMxQyxPQUFPLENBQUMsQ0FBQyxFQUFFLFVBQVUsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUN6QyxDQUFDO1FBQ0wsQ0FBQztJQUNMLENBQUM7SUFFRDs7O09BR0c7SUFDSSxNQUFNLENBQUMsc0JBQXNCO1FBRWhDLElBQUksbUJBQW1CLEdBQXVCLEVBQUUsQ0FBQztRQUNqRCxVQUFVLENBQUMsZUFBZSxDQUFDLFVBQVUsUUFBZ0IsRUFBRSxPQUFzQjtZQUV6RSxtQkFBbUIsQ0FBQyxJQUFJLENBQ3BCO2dCQUNJLFNBQVMsRUFBSSxPQUFPLENBQUMsV0FBVyxDQUFDLE1BQU0sRUFBRTtnQkFDekMsV0FBVyxFQUFFLE9BQU8sQ0FBQyxTQUFTLEVBQUU7YUFDbkMsQ0FDSixDQUFDO1FBQ04sQ0FBQyxDQUFDLENBQUM7UUFDSCxpQkFBTyxDQUFDLGdCQUFnQixDQUFDLEdBQUcsQ0FBQyxxQkFBcUIsRUFBRSxtQkFBbUIsQ0FBQyxDQUFDO0lBQzdFLENBQUM7SUFFTSxNQUFNLENBQUMsZUFBZSxDQUFDLE1BQWdCO1FBRTFDLElBQ0ksRUFBRSw4Q0FBOEM7UUFDNUMsTUFBTSxLQUFLLFNBQVM7O2dCQUVwQixNQUFNLENBQ1Q7O2dCQUVELENBQ0ksTUFBTSxLQUFLLFNBQVM7O3dCQUVwQixDQUNJLENBQUMsVUFBVSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxTQUFTLEVBQUU7O2dDQUVyQyxDQUFDLFVBQVUsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUMsU0FBUyxFQUFFLENBQ3hDLENBQ0osRUFDSCxDQUFDO1lBQ0MsVUFBVSxDQUFDLHFCQUFxQixHQUFHLElBQUksQ0FBQztZQUN4QyxVQUFVLENBQUMsZUFBZSxDQUFDLFVBQVUsUUFBZ0IsRUFBRSxPQUFzQjtnQkFFekUsd0RBQXdEO2dCQUN4RCxnR0FBZ0c7Z0JBQ2hHLE9BQU8sQ0FBQyxJQUFJLEVBQUUsQ0FBQyxDQUFDLCtGQUErRjtZQUNuSCxDQUFDLENBQUMsQ0FBQztZQUNILFVBQVUsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxFQUFFLENBQUM7WUFFakMsa0hBQWtIO1lBQ2xILFlBQVksQ0FBQyxVQUFVLENBQUMsMEJBQTBCLENBQUMsQ0FBQztZQUNwRCxVQUFVLENBQUMsMEJBQTBCLEdBQUcsVUFBVSxDQUFDO2dCQUUvQyxVQUFVLENBQUMscUJBQXFCLEdBQUcsS0FBSyxDQUFDO1lBQzdDLENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQztRQUNaLENBQUM7YUFBTSxDQUFDO1lBQ0osVUFBVSxDQUFDLGVBQWUsQ0FBQyxVQUFVLFFBQWdCLEVBQUUsT0FBc0I7Z0JBRXpFLE9BQU8sQ0FBQyxJQUFJLEVBQUUsQ0FBQztZQUNuQixDQUFDLENBQUMsQ0FBQztRQUNQLENBQUM7SUFDTCxDQUFDOztBQS9NTCxnQ0FpTkM7QUE1TWlCLHFCQUFVLEdBQW9CLEVBQUUsQ0FBQyxDQUFDLGtJQUFrSTtBQUNwSyx5QkFBYyxHQUFnQixJQUFJLENBQUM7QUFDbkMsMEJBQWUsR0FBZSxHQUFHLENBQUMifQ==