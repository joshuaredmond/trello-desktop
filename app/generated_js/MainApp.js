"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MainApp = void 0;
require("source-map-support/register.js"); // This is makes error messages use the .ts file path instead. Very handy.
const fs_extra_1 = __importDefault(require("fs-extra"));
const path_1 = __importDefault(require("path"));
const electron_store_1 = __importDefault(require("electron-store"));
const rollbar_1 = __importDefault(require("rollbar"));
const electron_1 = require("electron");
const SettingsWindow_1 = require("./SettingsWindow");
const MainWindow_1 = require("./MainWindow");
const MenuBar_1 = require("./MenuBar");
const TrayIcon_1 = require("./TrayIcon");
// @ts-ignore because this file is outside the ts_build folder.
// Allow eslint-disable because eslint hasn't caught up to the new assert syntax.
// eslint-disable-next-line @typescript-eslint/quotes
const package_json_1 = __importDefault(require("../../package.json"));
class MainApp {
    static init() {
        MainApp.initErrorReporting();
        if (process.platform == `win32`) {
            MainApp.platformType = `win`;
            MainApp.iconExtStr = `ico`;
        }
        else if (process.platform == `darwin`) {
            MainApp.platformType = `mac`;
            MainApp.iconExtStr = `icns`;
        }
        else {
            MainApp.platformType = `lin`;
            MainApp.iconExtStr = `png`;
        }
        MainApp.appPath = path_1.default.join(__dirname, `..`);
        MainApp.appIconPath = path_1.default.join(MainApp.appPath, `assets`, `icon.${MainApp.iconExtStr}`);
        MainApp.electronStoreObj = new electron_store_1.default({
            'name': `trelloDesktopConfig`,
            'clearInvalidConfig': true,
            'cwd': process.env.SNAP_USER_COMMON || electron_1.app.getPath(`userData`), // The snap common path stores data that's shared between all versions, so this way an update will keep the settings of an older version.
            'defaults': {
                'defaultListNum': 3,
                'isRunAtStartup': false,
                'isAddEnhancements': false,
                'windowPropertiesArr': [],
            }
        });
        MainApp.setRunAtStartup(MainApp.electronStoreObj.get(`isRunAtStartup`));
        MainApp.setupAppEvents();
        MainApp.setupIpc();
    }
    static setupAppEvents() {
        electron_1.app.on(`ready`, function () {
            MainApp.onReady();
        });
        electron_1.app.on(`window-all-closed`, function () {
            if (MainApp.platformType != `mac`) {
                electron_1.app.quit();
            }
        });
        electron_1.app.on(`before-quit`, function () {
            MainApp.beforeQuit();
        });
        MainApp.logMessageOnConsoleMessage();
    }
    static logMessageOnConsoleMessage() {
        electron_1.app.on(`web-contents-created`, function (evt, webContentsObj) {
            webContentsObj.on(`console-message`, function (evt, levelNum, messageStr) {
                if (!messageStr.includes(`Electron Security Warning (Insecure Content-Security-Policy)`)) {
                    var levelStr = ``;
                    if (levelNum == 0) {
                        levelStr = `verbose`;
                    }
                    else if (levelNum == 1) {
                        levelStr = `info`;
                    }
                    else if (levelNum == 2) {
                        levelStr = `warning`;
                    }
                    else if (levelNum == 3) {
                        levelStr = `error`;
                    }
                    console.log(`${webContentsObj.getTitle()} ${levelStr}: ${messageStr.replaceAll(`\n`, `\n    `)}`); // Keep for debugging.
                }
            });
        });
    }
    static initErrorReporting() {
        MainApp.isPackaged = electron_1.app.isPackaged;
        MainApp.packageJsonObj = package_json_1.default;
        // Error reporting.
        if (MainApp.isPackaged) {
            MainApp.rollbarObj = new rollbar_1.default({
                accessToken: `dd2f4f1d63ac40129c8d412ef77a878d`,
                captureUncaught: true,
                captureUnhandledRejections: true,
                codeVersion: MainApp.packageJsonObj.version,
                environment: (MainApp.isPackaged) ? `production` : `development`, // Will always be `production`, except when I enable it on dev for testing.
            });
        }
    }
    static onReady() {
        MainApp.setupSingleInstanceLock();
        MenuBar_1.MenuBar.init();
        MainWindow_1.MainWindow.init();
        SettingsWindow_1.SettingsWindow.init();
        TrayIcon_1.TrayIcon.init();
        MainApp.registerShortcuts();
    }
    static beforeQuit() {
        MainApp.isQuitting = true;
        MainWindow_1.MainWindow.setWindowPropertiesArr();
        TrayIcon_1.TrayIcon.trayIconObj.destroy();
    }
    static setRunAtStartup(isRunAtStartup) {
        if (MainApp.isPackaged) {
            if (MainApp.platformType == `lin`) {
                MainApp.setRunAtStartupLin(isRunAtStartup);
            }
            // Windows run at startup isn't built yet. See: `mod/electron/desktop/!Readme - Windows.md:33` (Look for "Run at startup". To open: [_Alt_]+[_Shift_]+[_N_])
        }
    }
    static setRunAtStartupLin(isRunAtStartup) {
        var autostartDesktopFilePath = `${process.env.SNAP_USER_DATA || process.env.HOME}/.config/autostart/${MainApp.packageJsonObj.name}.desktop`; // For some reason `os.userInfo().homedir` doesn't get the snap's home folder when installed from snap.
        var originalDesktopFilePath = ``;
        if (MainApp.electronStoreObj.get(`desktopFilePath`)) {
            originalDesktopFilePath = MainApp.electronStoreObj.get(`desktopFilePath`);
        }
        else {
            if (process.env.BAMF_DESKTOP_FILE_HINT) { // Snap sets this environment variable when the app is run from a .desktop file.
                // For .snap install.
                originalDesktopFilePath = process.env.BAMF_DESKTOP_FILE_HINT;
            }
            else {
                // For .deb install.
                originalDesktopFilePath = `/usr/share/applications/${MainApp.packageJsonObj.name}.desktop`;
            }
            MainApp.electronStoreObj.set(`desktopFilePath`, originalDesktopFilePath);
        }
        fs_extra_1.default.pathExists(originalDesktopFilePath, function (errorObj, isOriginalDesktopFileExists) {
            if (errorObj) {
                MainApp.electronStoreObj.set(`isRunAtStartup`, false);
                throw errorObj;
            }
            else if (isOriginalDesktopFileExists) {
                fs_extra_1.default.pathExists(autostartDesktopFilePath, function (errorObj, isAutostartDesktopFileExists) {
                    if (errorObj) {
                        MainApp.electronStoreObj.set(`isRunAtStartup`, false);
                        throw errorObj;
                    }
                    else {
                        if (isRunAtStartup) {
                            if (!isAutostartDesktopFileExists) {
                                fs_extra_1.default.ensureSymlink(originalDesktopFilePath, autostartDesktopFilePath, function (errorObj) {
                                    if (errorObj) {
                                        MainApp.electronStoreObj.set(`isRunAtStartup`, false); // Reset the setting so they can try again.
                                        throw errorObj;
                                    }
                                });
                            }
                        }
                        else {
                            if (isAutostartDesktopFileExists) {
                                fs_extra_1.default.unlink(autostartDesktopFilePath, function (errorObj) {
                                    if (errorObj) {
                                        MainApp.electronStoreObj.set(`isRunAtStartup`, true); // Reset the setting so they can try again.
                                        throw errorObj;
                                    }
                                });
                            }
                        }
                    }
                });
            }
        });
    }
    /*
     * When a second instance is opened quit the second instance and focus the first instance.
     */
    static setupSingleInstanceLock() {
        var isOnlyInstance = electron_1.app.requestSingleInstanceLock();
        if (isOnlyInstance) {
            electron_1.app.on(`second-instance`, function () {
                MainWindow_1.MainWindow.showHideMainWin(true);
            });
        }
        else {
            electron_1.app.quit();
        }
    }
    static registerShortcuts() {
        electron_1.globalShortcut.register(MainApp.shortcutStr_addCard, function () {
            MainWindow_1.MainWindow.showHideMainWin(true);
            MainWindow_1.MainWindow.windowsArr[0].webContents.send(`addCard`, MainApp.electronStoreObj.get(`defaultListNum`));
        });
        electron_1.globalShortcut.register(MainApp.shortcutStr_showHide, function () {
            MainWindow_1.MainWindow.showHideMainWin();
        });
        electron_1.globalShortcut.register(MainApp.shortcutStr_copyDoneList, function () {
            MainWindow_1.MainWindow.windowsArr[0].webContents.send(`get1stListText`);
        });
    }
    static setupIpc() {
        electron_1.ipcMain.on(`hideSettingsWin`, function () {
            SettingsWindow_1.SettingsWindow.settingsWin.hide();
        });
        electron_1.ipcMain.on(`copy1stListText`, function (ipcEvent, listText) {
            electron_1.clipboard.writeText(listText);
        });
        electron_1.ipcMain.on(`setStoreOption`, function (ipcEvent, optionObj) {
            MainApp.electronStoreObj.set(optionObj.itemKeyStr, optionObj.itemValueMixed);
            if (MainApp.isPackaged && optionObj.itemKeyStr == `isRunAtStartup`) {
                MainApp.setRunAtStartup(optionObj.itemValueMixed);
            }
            if (optionObj.itemKeyStr == `isAddEnhancements`) {
                MainWindow_1.MainWindow.runOnAllWindows(function (indexNum, mainWin) {
                    mainWin.reload();
                });
            }
        });
    }
}
exports.MainApp = MainApp;
MainApp.shortcutStr_addCard = `CmdOrCtrl+Alt+C`;
MainApp.shortcutStr_showHide = `CmdOrCtrl+Alt+X`;
MainApp.shortcutStr_copyDoneList = `CmdOrCtrl+Alt+.`;
MainApp.isQuitting = false;
MainApp.applicationUrl = `https://trello.com`;
MainApp.urlContainsAllowlistArr = [
    MainApp.applicationUrl,
    `atlassian`,
    `https://accounts.google.com/`,
    `okta.com`,
];
MainApp.init();
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiTWFpbkFwcC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uL3RzL01haW5BcHAudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7O0FBQUEsMENBQXdDLENBQUMsMEVBQTBFO0FBRW5ILHdEQUEwQjtBQUMxQixnREFBd0I7QUFDeEIsb0VBQTJDO0FBQzNDLHNEQUE4QjtBQUM5Qix1Q0FBZ0Y7QUFDaEYscURBQWdEO0FBQ2hELDZDQUF3QztBQUN4Qyx1Q0FBa0M7QUFDbEMseUNBQW9DO0FBRXBDLCtEQUErRDtBQUMvRCxpRkFBaUY7QUFDakYscURBQXFEO0FBQ3JELHNFQUFzRTtBQUV0RSxNQUFhLE9BQU87SUE0QlQsTUFBTSxDQUFDLElBQUk7UUFFZCxPQUFPLENBQUMsa0JBQWtCLEVBQUUsQ0FBQztRQUM3QixJQUFJLE9BQU8sQ0FBQyxRQUFRLElBQUksT0FBTyxFQUFFLENBQUM7WUFDOUIsT0FBTyxDQUFDLFlBQVksR0FBRyxLQUFLLENBQUM7WUFDN0IsT0FBTyxDQUFDLFVBQVUsR0FBSyxLQUFLLENBQUM7UUFDakMsQ0FBQzthQUFNLElBQUksT0FBTyxDQUFDLFFBQVEsSUFBSSxRQUFRLEVBQUUsQ0FBQztZQUN0QyxPQUFPLENBQUMsWUFBWSxHQUFHLEtBQUssQ0FBQztZQUM3QixPQUFPLENBQUMsVUFBVSxHQUFLLE1BQU0sQ0FBQztRQUNsQyxDQUFDO2FBQU0sQ0FBQztZQUNKLE9BQU8sQ0FBQyxZQUFZLEdBQUcsS0FBSyxDQUFDO1lBQzdCLE9BQU8sQ0FBQyxVQUFVLEdBQUssS0FBSyxDQUFDO1FBQ2pDLENBQUM7UUFDRCxPQUFPLENBQUMsT0FBTyxHQUFZLGNBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxDQUFDO1FBQ3RELE9BQU8sQ0FBQyxXQUFXLEdBQVEsY0FBSSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsT0FBTyxFQUFFLFFBQVEsRUFBRSxRQUFRLE9BQU8sQ0FBQyxVQUFVLEVBQUUsQ0FBQyxDQUFDO1FBQzlGLE9BQU8sQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLHdCQUFhLENBQWtCO1lBQzFELE1BQU0sRUFBZ0IscUJBQXFCO1lBQzNDLG9CQUFvQixFQUFFLElBQUk7WUFDMUIsS0FBSyxFQUFpQixPQUFPLENBQUMsR0FBRyxDQUFDLGdCQUFnQixJQUFJLGNBQUcsQ0FBQyxPQUFPLENBQUMsVUFBVSxDQUFDLEVBQUUseUlBQXlJO1lBQ3hOLFVBQVUsRUFBNkI7Z0JBQ25DLGdCQUFnQixFQUFPLENBQUM7Z0JBQ3hCLGdCQUFnQixFQUFPLEtBQUs7Z0JBQzVCLG1CQUFtQixFQUFJLEtBQUs7Z0JBQzVCLHFCQUFxQixFQUFFLEVBQUU7YUFDNUI7U0FDSixDQUFDLENBQUM7UUFFSCxPQUFPLENBQUMsZUFBZSxDQUFDLE9BQU8sQ0FBQyxnQkFBZ0IsQ0FBQyxHQUFHLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxDQUFDO1FBQ3hFLE9BQU8sQ0FBQyxjQUFjLEVBQUUsQ0FBQztRQUN6QixPQUFPLENBQUMsUUFBUSxFQUFFLENBQUM7SUFDdkIsQ0FBQztJQUVNLE1BQU0sQ0FBQyxjQUFjO1FBRXhCLGNBQUcsQ0FBQyxFQUFFLENBQUMsT0FBTyxFQUFFO1lBRVosT0FBTyxDQUFDLE9BQU8sRUFBRSxDQUFDO1FBQ3RCLENBQUMsQ0FBQyxDQUFDO1FBQ0gsY0FBRyxDQUFDLEVBQUUsQ0FBQyxtQkFBbUIsRUFBRTtZQUV4QixJQUFJLE9BQU8sQ0FBQyxZQUFZLElBQUksS0FBSyxFQUFFLENBQUM7Z0JBQ2hDLGNBQUcsQ0FBQyxJQUFJLEVBQUUsQ0FBQztZQUNmLENBQUM7UUFDTCxDQUFDLENBQUMsQ0FBQztRQUNILGNBQUcsQ0FBQyxFQUFFLENBQUMsYUFBYSxFQUFFO1lBRWxCLE9BQU8sQ0FBQyxVQUFVLEVBQUUsQ0FBQztRQUN6QixDQUFDLENBQUMsQ0FBQztRQUNILE9BQU8sQ0FBQywwQkFBMEIsRUFBRSxDQUFDO0lBQ3pDLENBQUM7SUFFTyxNQUFNLENBQUMsMEJBQTBCO1FBRXJDLGNBQUcsQ0FBQyxFQUFFLENBQUMsc0JBQXNCLEVBQUUsVUFBVSxHQUFtQixFQUFFLGNBQW9DO1lBRTlGLGNBQWMsQ0FBQyxFQUFFLENBQUMsaUJBQWlCLEVBQUUsVUFBVSxHQUFtQixFQUFFLFFBQWdCLEVBQUUsVUFBa0I7Z0JBRXBHLElBQUksQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLDhEQUE4RCxDQUFDLEVBQUUsQ0FBQztvQkFDdkYsSUFBSSxRQUFRLEdBQUcsRUFBRSxDQUFDO29CQUNsQixJQUFJLFFBQVEsSUFBSSxDQUFDLEVBQUUsQ0FBQzt3QkFDaEIsUUFBUSxHQUFHLFNBQVMsQ0FBQztvQkFDekIsQ0FBQzt5QkFBTSxJQUFJLFFBQVEsSUFBSSxDQUFDLEVBQUUsQ0FBQzt3QkFDdkIsUUFBUSxHQUFHLE1BQU0sQ0FBQztvQkFDdEIsQ0FBQzt5QkFBTSxJQUFJLFFBQVEsSUFBSSxDQUFDLEVBQUUsQ0FBQzt3QkFDdkIsUUFBUSxHQUFHLFNBQVMsQ0FBQztvQkFDekIsQ0FBQzt5QkFBTSxJQUFJLFFBQVEsSUFBSSxDQUFDLEVBQUUsQ0FBQzt3QkFDdkIsUUFBUSxHQUFHLE9BQU8sQ0FBQztvQkFDdkIsQ0FBQztvQkFDRCxPQUFPLENBQUMsR0FBRyxDQUFDLEdBQUcsY0FBYyxDQUFDLFFBQVEsRUFBRSxJQUFJLFFBQVEsS0FBSyxVQUFVLENBQUMsVUFBVSxDQUFDLElBQUksRUFBRSxRQUFRLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxzQkFBc0I7Z0JBQzdILENBQUM7WUFDTCxDQUFDLENBQUMsQ0FBQztRQUNQLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQztJQUVPLE1BQU0sQ0FBQyxrQkFBa0I7UUFFN0IsT0FBTyxDQUFDLFVBQVUsR0FBTyxjQUFHLENBQUMsVUFBVSxDQUFDO1FBQ3hDLE9BQU8sQ0FBQyxjQUFjLEdBQUcsc0JBQWMsQ0FBQztRQUV4QyxtQkFBbUI7UUFDbkIsSUFBSSxPQUFPLENBQUMsVUFBVSxFQUFFLENBQUM7WUFDckIsT0FBTyxDQUFDLFVBQVUsR0FBRyxJQUFJLGlCQUFPLENBQUM7Z0JBQzdCLFdBQVcsRUFBaUIsa0NBQWtDO2dCQUM5RCxlQUFlLEVBQWEsSUFBSTtnQkFDaEMsMEJBQTBCLEVBQUUsSUFBSTtnQkFDaEMsV0FBVyxFQUFpQixPQUFPLENBQUMsY0FBYyxDQUFDLE9BQU87Z0JBQzFELFdBQVcsRUFBaUIsQ0FBQyxPQUFPLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUMsYUFBYSxFQUFFLDJFQUEyRTthQUMvSixDQUFDLENBQUM7UUFDUCxDQUFDO0lBQ0wsQ0FBQztJQUVPLE1BQU0sQ0FBQyxPQUFPO1FBRWxCLE9BQU8sQ0FBQyx1QkFBdUIsRUFBRSxDQUFDO1FBQ2xDLGlCQUFPLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDZix1QkFBVSxDQUFDLElBQUksRUFBRSxDQUFDO1FBQ2xCLCtCQUFjLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDdEIsbUJBQVEsQ0FBQyxJQUFJLEVBQUUsQ0FBQztRQUNoQixPQUFPLENBQUMsaUJBQWlCLEVBQUUsQ0FBQztJQUNoQyxDQUFDO0lBRU0sTUFBTSxDQUFDLFVBQVU7UUFFcEIsT0FBTyxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUM7UUFDMUIsdUJBQVUsQ0FBQyxzQkFBc0IsRUFBRSxDQUFDO1FBQ3BDLG1CQUFRLENBQUMsV0FBVyxDQUFDLE9BQU8sRUFBRSxDQUFDO0lBQ25DLENBQUM7SUFFTSxNQUFNLENBQUMsZUFBZSxDQUFDLGNBQXVCO1FBRWpELElBQUksT0FBTyxDQUFDLFVBQVUsRUFBRSxDQUFDO1lBQ3JCLElBQUksT0FBTyxDQUFDLFlBQVksSUFBSSxLQUFLLEVBQUUsQ0FBQztnQkFDaEMsT0FBTyxDQUFDLGtCQUFrQixDQUFDLGNBQWMsQ0FBQyxDQUFDO1lBQy9DLENBQUM7WUFDRCw0SkFBNEo7UUFDaEssQ0FBQztJQUNMLENBQUM7SUFFTyxNQUFNLENBQUMsa0JBQWtCLENBQUMsY0FBdUI7UUFFckQsSUFBSSx3QkFBd0IsR0FBRyxHQUFHLE9BQU8sQ0FBQyxHQUFHLENBQUMsY0FBYyxJQUFJLE9BQU8sQ0FBQyxHQUFHLENBQUMsSUFBSSxzQkFBc0IsT0FBTyxDQUFDLGNBQWMsQ0FBQyxJQUFJLFVBQVUsQ0FBQyxDQUFDLHVHQUF1RztRQUNwUCxJQUFJLHVCQUF1QixHQUFJLEVBQUUsQ0FBQztRQUVsQyxJQUFJLE9BQU8sQ0FBQyxnQkFBZ0IsQ0FBQyxHQUFHLENBQUMsaUJBQWlCLENBQUMsRUFBRSxDQUFDO1lBQ2xELHVCQUF1QixHQUFHLE9BQU8sQ0FBQyxnQkFBZ0IsQ0FBQyxHQUFHLENBQUMsaUJBQWlCLENBQUMsQ0FBQztRQUM5RSxDQUFDO2FBQU0sQ0FBQztZQUNKLElBQUksT0FBTyxDQUFDLEdBQUcsQ0FBQyxzQkFBc0IsRUFBRSxDQUFDLENBQUMsZ0ZBQWdGO2dCQUN0SCxxQkFBcUI7Z0JBQ3JCLHVCQUF1QixHQUFHLE9BQU8sQ0FBQyxHQUFHLENBQUMsc0JBQXNCLENBQUM7WUFDakUsQ0FBQztpQkFBTSxDQUFDO2dCQUNKLG9CQUFvQjtnQkFDcEIsdUJBQXVCLEdBQUcsMkJBQTJCLE9BQU8sQ0FBQyxjQUFjLENBQUMsSUFBSSxVQUFVLENBQUM7WUFDL0YsQ0FBQztZQUNELE9BQU8sQ0FBQyxnQkFBZ0IsQ0FBQyxHQUFHLENBQUMsaUJBQWlCLEVBQUUsdUJBQXVCLENBQUMsQ0FBQztRQUM3RSxDQUFDO1FBRUQsa0JBQUUsQ0FBQyxVQUFVLENBQUMsdUJBQXVCLEVBQUUsVUFBVSxRQUFlLEVBQUUsMkJBQW9DO1lBRWxHLElBQUksUUFBUSxFQUFFLENBQUM7Z0JBQ1gsT0FBTyxDQUFDLGdCQUFnQixDQUFDLEdBQUcsQ0FBQyxnQkFBZ0IsRUFBRSxLQUFLLENBQUMsQ0FBQztnQkFDdEQsTUFBTSxRQUFRLENBQUM7WUFDbkIsQ0FBQztpQkFBTSxJQUFJLDJCQUEyQixFQUFFLENBQUM7Z0JBQ3JDLGtCQUFFLENBQUMsVUFBVSxDQUFDLHdCQUF3QixFQUFFLFVBQVUsUUFBZSxFQUFFLDRCQUFxQztvQkFFcEcsSUFBSSxRQUFRLEVBQUUsQ0FBQzt3QkFDWCxPQUFPLENBQUMsZ0JBQWdCLENBQUMsR0FBRyxDQUFDLGdCQUFnQixFQUFFLEtBQUssQ0FBQyxDQUFDO3dCQUN0RCxNQUFNLFFBQVEsQ0FBQztvQkFDbkIsQ0FBQzt5QkFBTSxDQUFDO3dCQUNKLElBQUksY0FBYyxFQUFFLENBQUM7NEJBQ2pCLElBQUksQ0FBQyw0QkFBNEIsRUFBRSxDQUFDO2dDQUNoQyxrQkFBRSxDQUFDLGFBQWEsQ0FBQyx1QkFBdUIsRUFBRSx3QkFBd0IsRUFBRSxVQUFVLFFBQWU7b0NBRXpGLElBQUksUUFBUSxFQUFFLENBQUM7d0NBQ1gsT0FBTyxDQUFDLGdCQUFnQixDQUFDLEdBQUcsQ0FBQyxnQkFBZ0IsRUFBRSxLQUFLLENBQUMsQ0FBQyxDQUFDLDJDQUEyQzt3Q0FDbEcsTUFBTSxRQUFRLENBQUM7b0NBQ25CLENBQUM7Z0NBQ0wsQ0FBQyxDQUFDLENBQUM7NEJBQ1AsQ0FBQzt3QkFDTCxDQUFDOzZCQUFNLENBQUM7NEJBQ0osSUFBSSw0QkFBNEIsRUFBRSxDQUFDO2dDQUMvQixrQkFBRSxDQUFDLE1BQU0sQ0FBQyx3QkFBd0IsRUFBRSxVQUFVLFFBQWU7b0NBRXpELElBQUksUUFBUSxFQUFFLENBQUM7d0NBQ1gsT0FBTyxDQUFDLGdCQUFnQixDQUFDLEdBQUcsQ0FBQyxnQkFBZ0IsRUFBRSxJQUFJLENBQUMsQ0FBQyxDQUFDLDJDQUEyQzt3Q0FDakcsTUFBTSxRQUFRLENBQUM7b0NBQ25CLENBQUM7Z0NBQ0wsQ0FBQyxDQUFDLENBQUM7NEJBQ1AsQ0FBQzt3QkFDTCxDQUFDO29CQUNMLENBQUM7Z0JBQ0wsQ0FBQyxDQUFDLENBQUM7WUFDUCxDQUFDO1FBQ0wsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQ7O09BRUc7SUFDSSxNQUFNLENBQUMsdUJBQXVCO1FBRWpDLElBQUksY0FBYyxHQUFHLGNBQUcsQ0FBQyx5QkFBeUIsRUFBRSxDQUFDO1FBQ3JELElBQUksY0FBYyxFQUFFLENBQUM7WUFDakIsY0FBRyxDQUFDLEVBQUUsQ0FBQyxpQkFBaUIsRUFBRTtnQkFFdEIsdUJBQVUsQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDckMsQ0FBQyxDQUFDLENBQUM7UUFDUCxDQUFDO2FBQU0sQ0FBQztZQUNKLGNBQUcsQ0FBQyxJQUFJLEVBQUUsQ0FBQztRQUNmLENBQUM7SUFDTCxDQUFDO0lBRU0sTUFBTSxDQUFDLGlCQUFpQjtRQUUzQix5QkFBYyxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsbUJBQW1CLEVBQUU7WUFFakQsdUJBQVUsQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDakMsdUJBQVUsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxTQUFTLEVBQUUsT0FBTyxDQUFDLGdCQUFnQixDQUFDLEdBQUcsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLENBQUM7UUFDekcsQ0FBQyxDQUFDLENBQUM7UUFDSCx5QkFBYyxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsb0JBQW9CLEVBQUU7WUFFbEQsdUJBQVUsQ0FBQyxlQUFlLEVBQUUsQ0FBQztRQUNqQyxDQUFDLENBQUMsQ0FBQztRQUNILHlCQUFjLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyx3QkFBd0IsRUFBRTtZQUV0RCx1QkFBVSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLENBQUM7UUFDaEUsQ0FBQyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRU0sTUFBTSxDQUFDLFFBQVE7UUFFbEIsa0JBQU8sQ0FBQyxFQUFFLENBQUMsaUJBQWlCLEVBQUU7WUFFMUIsK0JBQWMsQ0FBQyxXQUFXLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDdEMsQ0FBQyxDQUFDLENBQUM7UUFDSCxrQkFBTyxDQUFDLEVBQUUsQ0FBQyxpQkFBaUIsRUFBRSxVQUFVLFFBQVEsRUFBRSxRQUFRO1lBRXRELG9CQUFTLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQ2xDLENBQUMsQ0FBQyxDQUFDO1FBQ0gsa0JBQU8sQ0FBQyxFQUFFLENBQUMsZ0JBQWdCLEVBQUUsVUFBVSxRQUFRLEVBQUUsU0FBUztZQUV0RCxPQUFPLENBQUMsZ0JBQWdCLENBQUMsR0FBRyxDQUFDLFNBQVMsQ0FBQyxVQUFVLEVBQUUsU0FBUyxDQUFDLGNBQWMsQ0FBQyxDQUFDO1lBQzdFLElBQUksT0FBTyxDQUFDLFVBQVUsSUFBSSxTQUFTLENBQUMsVUFBVSxJQUFJLGdCQUFnQixFQUFFLENBQUM7Z0JBQ2pFLE9BQU8sQ0FBQyxlQUFlLENBQUMsU0FBUyxDQUFDLGNBQWMsQ0FBQyxDQUFDO1lBQ3RELENBQUM7WUFDRCxJQUFJLFNBQVMsQ0FBQyxVQUFVLElBQUksbUJBQW1CLEVBQUUsQ0FBQztnQkFDOUMsdUJBQVUsQ0FBQyxlQUFlLENBQUMsVUFBVSxRQUFnQixFQUFFLE9BQXNCO29CQUV6RSxPQUFPLENBQUMsTUFBTSxFQUFFLENBQUM7Z0JBQ3JCLENBQUMsQ0FBQyxDQUFDO1lBQ1AsQ0FBQztRQUNMLENBQUMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQzs7QUFuUUwsMEJBcVFDO0FBbFFpQiwyQkFBbUIsR0FBZ0IsaUJBQWlCLENBQUM7QUFDckQsNEJBQW9CLEdBQWUsaUJBQWlCLENBQUM7QUFDckQsZ0NBQXdCLEdBQVcsaUJBQWlCLENBQUM7QUFhckQsa0JBQVUsR0FBeUIsS0FBSyxDQUFDO0FBQ3pDLHNCQUFjLEdBQXFCLG9CQUFvQixDQUFDO0FBQ3hELCtCQUF1QixHQUFPO0lBQ3hDLE9BQU8sQ0FBQyxjQUFjO0lBQ3RCLFdBQVc7SUFDWCw4QkFBOEI7SUFDOUIsVUFBVTtDQUNiLENBQUM7QUE4T04sT0FBTyxDQUFDLElBQUksRUFBRSxDQUFDIn0=