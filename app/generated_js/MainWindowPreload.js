"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const electron_1 = require("electron");
const fs_1 = __importDefault(require("fs"));
const path_1 = __importDefault(require("path"));
window.addEventListener(`load`, function () {
    const $ = require(`jquery`);
    electron_1.ipcRenderer.invoke(`isAddEnhancements`).then(function (isAddEnhancements) {
        if (isAddEnhancements) {
            addEnhancements();
        }
    });
    electron_1.ipcRenderer.on(`addCard`, function (evt, defaultListNum) {
        // New.
        const $cardTextareaJq = $(`#board [data-testid="list-wrapper"]:nth-child(${defaultListNum}) [data-testid="list-card-composer-textarea"]`);
        if ($cardTextareaJq.length) { // If composer is open already, then focus.
            $cardTextareaJq.get(0).focus();
        }
        else {
            var $addCardButtonJq = $(`#board [data-testid="list-wrapper"]:nth-child(${defaultListNum}) [data-testid="list-add-card-button"]`);
            if (!$addCardButtonJq.length) { // Add to last list if default list isn't found.
                $addCardButtonJq = $(`#board [data-testid="list-wrapper"]:last [data-testid="list-add-card-button"]`);
            }
            $addCardButtonJq.get(0).click();
        }
        // Old.
        // var $addCardButtonJq = $(`#board .js-list:nth-child(${defaultListNum}) .list .open-card-composer`);
        // if (!$addCardButtonJq.length) { // Add to last list if default list isn't found.
        //     $addCardButtonJq = $(`#board .js-list:last .list .open-card-composer`);
        // }
        // // If you can add a card then press the button.
        // if ($addCardButtonJq.is(`:visible`)) {
        //     $addCardButtonJq.get(0).click();
        // } else { // Or if the button has already been pressed then just focus the textarea.
        //     $(`.list-card-composer-textarea`).get(0).focus();
        // }
    });
    electron_1.ipcRenderer.on(`get1stListText`, function () {
        // New.
        var listText = ``;
        const $firstListWrapperJq = $(`[data-testid="list-wrapper"]:first`);
        var titleStr = `Nothing to copy`;
        var listHeading = $firstListWrapperJq.find(`[data-testid="list-name"]`).text();
        if (listHeading) {
            titleStr = `Copied "${listHeading}"`;
            let cardCount = 1;
            $firstListWrapperJq.find(`[data-testid='card-name']`).each(function (i, cardEl) {
                listText += `${cardCount++}. ${cardHeading($(cardEl).parent().get(0))}\n`;
            });
            electron_1.ipcRenderer.send(`copy1stListText`, listText);
        }
        // Old.
        // var listText     = ``;
        // var $firstListJq = $(`.list:first`);
        // var titleStr     = `Nothing to copy`;
        // var listHeading  = $firstListJq
        //     .find(`.list-header`)
        //     .first()
        //     .find(`h2`)
        //     .first()
        //     .html();
        // if (listHeading) {
        //     titleStr = `Copied "${listHeading}"`;
        //     let cardCount = 1;
        //     $firstListJq.find(`.list-card-details`).each(function (i: number, cardEl: HTMLElement): void
        //     {
        //         listText += `${cardCount++}. ${cardHeading(cardEl)}\n`;
        //     });
        //     ipcRenderer.send(`copy1stListText`, listText);
        // }
        new Notification(titleStr, {
            // Icon is base64 of: `icon32x32 (converted to base64 for notifications).png`
            icon: `data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAMAAABEpIrGAAABd1BMVEUAAAAjgPsjgfwDVc4DVc8jgPsjgPsDVc8DVtAjgPskgv0DVtACVc8kgfwjgPsDVtEQcu4TdPAUdfEWd/NBfe4BVM4kgv1MfuhGhve5xfHo7P3s8P0kgv1Ug+pVg+tVhe1XjfdhgN28y/rj5/kBVM4devUhfvkQaeMee/YuZ9vCz/oEV9IQaeMee/Ykgv06a9qbqubU2vP6+v0BVM4CVdADVtAEV9IFWNMFWdMGWtUGW9UHXNYHXdcIXtgJX9oKYNsLYNsLYdsLYdwMYt0OZN8PZeAQZ+EQZ+IQauQRaeMSauQTbOYTbOcUbegUbukVbukWcOsXcOsYcu0Zc+4adfAbdfAbdvEbd/IdePMdefQdevUeevUfe/YffPcgffghffghfvkhf/ojgPskgv1fgOBfgeFfguNgg+RgheVghuZghudgiOlgiethiexhi+5hjO9hjfBijvJij/NikPWjsOfo6vno6/no6/ro6/vo7Pvo7Pzo7P3///8J3cRZAAAAM3RSTlMANzc6Oz4/QkS+vsDDw8fN8PDw8fb3+Pj5+fn5+vr6+vr6+vr7/Pz9/f39/v7+/v7+/v4ghur3AAABYUlEQVQ4y33KB1fCMBQF4OteuBfuvRX3AhmKigpuqKgVQdx7g8CPNy9NPEJP+dLT5L53gYJyS9SApTQXKGyNZtGQg4qLrEoQDkfYCadfkb+rH6E056HMDFU9Y2ewk5hVnsw8jNFcVXFK+lpSXDeFYe3dfMJXCDLHH9oslaSQFKGWVkEckXcxS1BIiFBDQYFfCfiVN1kIKH5FFqoDBIfkVRYoyEIVX+GAvMgCBVmo4yvsk2cx+6EQF6GDwh52yZOYxSnIQjtfYZs8ysIOC7LQxlfwMr4HWfD6fN6YCI0+xostci9mMQpN2rtng6+wSe5kgafRAbKpgcfjWffcysI6w/I/WCM3ovC9poNVci0LqzpwkytZcOvASS5F4cu1vOJyOQUXgYMMTWmFSYcO7FzXJ5kYt+vAplmyGYDVal3knwEsMPOM/GcmzGU3gsrZDDPy5o8yFPdOZ1GfBxSZjPemfPwCSEpHU6FuQrkAAAAASUVORK5CYII=`,
            body: listText,
        });
    });
    // New.
    // Taken from: ZJS.Crx.Trello.cardHeading
    function cardHeading(cardEl) {
        var $cardHeadingJq = $(cardEl).find(`a[data-testid='card-name']`).first();
        var cardHeading = $cardHeadingJq.text();
        if (cardHeading) {
            if (cardHeading.startsWith(`#`)) {
                var cardId = $cardHeadingJq
                    .find(`span`)
                    .first()
                    .text();
                cardHeading = cardHeading.replace(cardId, ``);
            }
            var cardCountStr = $cardHeadingJq
                .find(`.cardCounter_appsBrand`)
                .last()
                .text();
            if (cardCountStr) {
                cardHeading = cardHeading.replace(new RegExp(`${cardCountStr}$`), ``);
            }
            return cardHeading;
        }
        else {
            return ``;
        }
    }
    // Old.
    // // Taken from: ZJS.Crx.Trello.cardHeading
    // function cardHeading(cardEl: HTMLElement): string
    // {
    //     var $cardHeadingJq = $(cardEl).find(`.list-card-title`).first();
    //     var cardHeading    = $cardHeadingJq.text();
    //     if (cardHeading) {
    //         if (cardHeading.startsWith(`#`)) {
    //             var cardId  = $cardHeadingJq
    //                 .find(`span`)
    //                 .first()
    //                 .text();
    //             cardHeading = cardHeading.replace(cardId, ``);
    //         }
    //         var cardCountStr = $cardHeadingJq
    //             .find(`.cardCounter_appsBrand`)
    //             .last()
    //             .text();
    //         if (cardCountStr) {
    //             cardHeading = cardHeading.replace(new RegExp(`${cardCountStr}$`), ``);
    //         }
    //         return cardHeading;
    //     } else {
    //         return ``;
    //     }
    // }
});
function addEnhancements() {
    fs_1.default.readFile(path_1.default.join(__dirname, `..`, `ext`, `generated_colors_lgt.css`), `utf8`, function (errorObj, trelloStyleCss) {
        if (errorObj) {
            return console.log(errorObj);
        }
        else {
            var styleEl = document.createElement(`style`);
            document.head.appendChild(styleEl);
            styleEl.appendChild(document.createTextNode(trelloStyleCss));
        }
    });
    fs_1.default.readFile(path_1.default.join(__dirname, `..`, `ext`, `trello.css`), `utf8`, function (errorObj, trelloStyleCss) {
        if (errorObj) {
            return console.log(errorObj);
        }
        else {
            var styleEl = document.createElement(`style`);
            document.head.appendChild(styleEl);
            styleEl.appendChild(document.createTextNode(trelloStyleCss));
        }
    });
    fs_1.default.readFile(path_1.default.join(__dirname, `..`, `ext`, `TrelloInject.js`), `utf8`, function (errorObj, trelloJs) {
        if (errorObj) {
            return console.log(errorObj);
        }
        else {
            var scriptEl = document.createElement(`script`);
            document.head.appendChild(scriptEl);
            scriptEl.appendChild(document.createTextNode(trelloJs));
        }
    });
    fs_1.default.readFile(path_1.default.join(__dirname, `..`, `ext`, `Trello.js`), `utf8`, function (errorObj, trelloJs) {
        if (errorObj) {
            return console.log(errorObj);
        }
        else {
            var scriptEl = document.createElement(`script`);
            document.head.appendChild(scriptEl);
            scriptEl.appendChild(document.createTextNode(trelloJs));
        }
    });
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiTWFpbldpbmRvd1ByZWxvYWQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi90cy9NYWluV2luZG93UHJlbG9hZC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLHVDQUF1RDtBQUN2RCw0Q0FBb0I7QUFDcEIsZ0RBQXdCO0FBRXhCLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxNQUFNLEVBQUU7SUFFNUIsTUFBTSxDQUFDLEdBQUcsT0FBTyxDQUFDLFFBQVEsQ0FBQyxDQUFDO0lBQzVCLHNCQUFXLENBQUMsTUFBTSxDQUFDLG1CQUFtQixDQUFDLENBQUMsSUFBSSxDQUFDLFVBQVUsaUJBQTBCO1FBRTdFLElBQUksaUJBQWlCLEVBQUUsQ0FBQztZQUNwQixlQUFlLEVBQUUsQ0FBQztRQUN0QixDQUFDO0lBQ0wsQ0FBQyxDQUFDLENBQUM7SUFDSCxzQkFBVyxDQUFDLEVBQUUsQ0FBQyxTQUFTLEVBQUUsVUFBVSxHQUFxQixFQUFFLGNBQXNCO1FBRTdFLE9BQU87UUFDUCxNQUFNLGVBQWUsR0FBRyxDQUFDLENBQUMsaURBQWlELGNBQWMsK0NBQStDLENBQUMsQ0FBQztRQUMxSSxJQUFJLGVBQWUsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxDQUFDLDJDQUEyQztZQUNyRSxlQUFlLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssRUFBRSxDQUFDO1FBQ25DLENBQUM7YUFBTSxDQUFDO1lBQ0osSUFBSSxnQkFBZ0IsR0FBRyxDQUFDLENBQUMsaURBQWlELGNBQWMsd0NBQXdDLENBQUMsQ0FBQztZQUNsSSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsTUFBTSxFQUFFLENBQUMsQ0FBQyxnREFBZ0Q7Z0JBQzVFLGdCQUFnQixHQUFHLENBQUMsQ0FBQywrRUFBK0UsQ0FBQyxDQUFDO1lBQzFHLENBQUM7WUFDRCxnQkFBZ0IsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxFQUFFLENBQUM7UUFDcEMsQ0FBQztRQUVELE9BQU87UUFDUCxzR0FBc0c7UUFDdEcsbUZBQW1GO1FBQ25GLDhFQUE4RTtRQUM5RSxJQUFJO1FBRUosa0RBQWtEO1FBQ2xELHlDQUF5QztRQUN6Qyx1Q0FBdUM7UUFDdkMsc0ZBQXNGO1FBQ3RGLHdEQUF3RDtRQUN4RCxJQUFJO0lBQ1IsQ0FBQyxDQUFDLENBQUM7SUFDSCxzQkFBVyxDQUFDLEVBQUUsQ0FBQyxnQkFBZ0IsRUFBRTtRQUU3QixPQUFPO1FBQ1AsSUFBSSxRQUFRLEdBQU8sRUFBRSxDQUFDO1FBQ3RCLE1BQU0sbUJBQW1CLEdBQUcsQ0FBQyxDQUFDLG9DQUFvQyxDQUFDLENBQUM7UUFDcEUsSUFBSSxRQUFRLEdBQU8saUJBQWlCLENBQUM7UUFDckMsSUFBSSxXQUFXLEdBQUksbUJBQW1CLENBQUMsSUFBSSxDQUFDLDJCQUEyQixDQUFDLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDaEYsSUFBSSxXQUFXLEVBQUUsQ0FBQztZQUNkLFFBQVEsR0FBRyxXQUFXLFdBQVcsR0FBRyxDQUFDO1lBQ3JDLElBQUksU0FBUyxHQUFHLENBQUMsQ0FBQztZQUNsQixtQkFBbUIsQ0FBQyxJQUFJLENBQUMsMkJBQTJCLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFTLEVBQUUsTUFBbUI7Z0JBRS9GLFFBQVEsSUFBSSxHQUFHLFNBQVMsRUFBRSxLQUFLLFdBQVcsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUMsTUFBTSxFQUFFLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztZQUM5RSxDQUFDLENBQUMsQ0FBQztZQUNILHNCQUFXLENBQUMsSUFBSSxDQUFDLGlCQUFpQixFQUFFLFFBQVEsQ0FBQyxDQUFDO1FBQ2xELENBQUM7UUFFRCxPQUFPO1FBQ1AseUJBQXlCO1FBQ3pCLHVDQUF1QztRQUN2Qyx3Q0FBd0M7UUFDeEMsa0NBQWtDO1FBQ2xDLDRCQUE0QjtRQUM1QixlQUFlO1FBQ2Ysa0JBQWtCO1FBQ2xCLGVBQWU7UUFDZixlQUFlO1FBQ2YscUJBQXFCO1FBQ3JCLDRDQUE0QztRQUM1Qyx5QkFBeUI7UUFDekIsbUdBQW1HO1FBQ25HLFFBQVE7UUFDUixrRUFBa0U7UUFDbEUsVUFBVTtRQUNWLHFEQUFxRDtRQUNyRCxJQUFJO1FBQ0osSUFBSSxZQUFZLENBQUMsUUFBUSxFQUFFO1lBQ3ZCLDZFQUE2RTtZQUM3RSxJQUFJLEVBQUUsb3BDQUFvcEM7WUFDMXBDLElBQUksRUFBRSxRQUFRO1NBQ2pCLENBQUMsQ0FBQztJQUNQLENBQUMsQ0FBQyxDQUFDO0lBRUgsT0FBTztJQUNQLHlDQUF5QztJQUN6QyxTQUFTLFdBQVcsQ0FBQyxNQUFtQjtRQUVwQyxJQUFJLGNBQWMsR0FBRyxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUMsSUFBSSxDQUFDLDRCQUE0QixDQUFDLENBQUMsS0FBSyxFQUFFLENBQUM7UUFDMUUsSUFBSSxXQUFXLEdBQU0sY0FBYyxDQUFDLElBQUksRUFBRSxDQUFDO1FBQzNDLElBQUksV0FBVyxFQUFFLENBQUM7WUFDZCxJQUFJLFdBQVcsQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQztnQkFDOUIsSUFBSSxNQUFNLEdBQUksY0FBYztxQkFDdkIsSUFBSSxDQUFDLE1BQU0sQ0FBQztxQkFDWixLQUFLLEVBQUU7cUJBQ1AsSUFBSSxFQUFFLENBQUM7Z0JBQ1osV0FBVyxHQUFHLFdBQVcsQ0FBQyxPQUFPLENBQUMsTUFBTSxFQUFFLEVBQUUsQ0FBQyxDQUFDO1lBQ2xELENBQUM7WUFDRCxJQUFJLFlBQVksR0FBRyxjQUFjO2lCQUM1QixJQUFJLENBQUMsd0JBQXdCLENBQUM7aUJBQzlCLElBQUksRUFBRTtpQkFDTixJQUFJLEVBQUUsQ0FBQztZQUNaLElBQUksWUFBWSxFQUFFLENBQUM7Z0JBQ2YsV0FBVyxHQUFHLFdBQVcsQ0FBQyxPQUFPLENBQUMsSUFBSSxNQUFNLENBQUMsR0FBRyxZQUFZLEdBQUcsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUFDO1lBQzFFLENBQUM7WUFDRCxPQUFPLFdBQVcsQ0FBQztRQUN2QixDQUFDO2FBQU0sQ0FBQztZQUNKLE9BQU8sRUFBRSxDQUFDO1FBQ2QsQ0FBQztJQUNMLENBQUM7SUFFRCxPQUFPO0lBQ1AsNENBQTRDO0lBQzVDLG9EQUFvRDtJQUNwRCxJQUFJO0lBQ0osdUVBQXVFO0lBQ3ZFLGtEQUFrRDtJQUNsRCx5QkFBeUI7SUFDekIsNkNBQTZDO0lBQzdDLDJDQUEyQztJQUMzQyxnQ0FBZ0M7SUFDaEMsMkJBQTJCO0lBQzNCLDJCQUEyQjtJQUMzQiw2REFBNkQ7SUFDN0QsWUFBWTtJQUNaLDRDQUE0QztJQUM1Qyw4Q0FBOEM7SUFDOUMsc0JBQXNCO0lBQ3RCLHVCQUF1QjtJQUN2Qiw4QkFBOEI7SUFDOUIscUZBQXFGO0lBQ3JGLFlBQVk7SUFDWiw4QkFBOEI7SUFDOUIsZUFBZTtJQUNmLHFCQUFxQjtJQUNyQixRQUFRO0lBQ1IsSUFBSTtBQUNSLENBQUMsQ0FBQyxDQUFDO0FBRUgsU0FBUyxlQUFlO0lBRXBCLFlBQUUsQ0FBQyxRQUFRLENBQUMsY0FBSSxDQUFDLElBQUksQ0FBQyxTQUFTLEVBQUUsSUFBSSxFQUFFLEtBQUssRUFBRSwwQkFBMEIsQ0FBQyxFQUFFLE1BQU0sRUFBRSxVQUFVLFFBQVEsRUFBRSxjQUFjO1FBRWpILElBQUksUUFBUSxFQUFFLENBQUM7WUFDWCxPQUFPLE9BQU8sQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLENBQUM7UUFDakMsQ0FBQzthQUFNLENBQUM7WUFDSixJQUFJLE9BQU8sR0FBRyxRQUFRLENBQUMsYUFBYSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBRTlDLFFBQVEsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBQ25DLE9BQU8sQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLGNBQWMsQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDO1FBQ2pFLENBQUM7SUFDTCxDQUFDLENBQUMsQ0FBQztJQUNILFlBQUUsQ0FBQyxRQUFRLENBQUMsY0FBSSxDQUFDLElBQUksQ0FBQyxTQUFTLEVBQUUsSUFBSSxFQUFFLEtBQUssRUFBRSxZQUFZLENBQUMsRUFBRSxNQUFNLEVBQUUsVUFBVSxRQUFRLEVBQUUsY0FBYztRQUVuRyxJQUFJLFFBQVEsRUFBRSxDQUFDO1lBQ1gsT0FBTyxPQUFPLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQ2pDLENBQUM7YUFBTSxDQUFDO1lBQ0osSUFBSSxPQUFPLEdBQUcsUUFBUSxDQUFDLGFBQWEsQ0FBQyxPQUFPLENBQUMsQ0FBQztZQUU5QyxRQUFRLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUMsQ0FBQztZQUNuQyxPQUFPLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxjQUFjLENBQUMsY0FBYyxDQUFDLENBQUMsQ0FBQztRQUNqRSxDQUFDO0lBQ0wsQ0FBQyxDQUFDLENBQUM7SUFFSCxZQUFFLENBQUMsUUFBUSxDQUFDLGNBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFLElBQUksRUFBRSxLQUFLLEVBQUUsaUJBQWlCLENBQUMsRUFBRSxNQUFNLEVBQUUsVUFBVSxRQUFRLEVBQUUsUUFBUTtRQUVsRyxJQUFJLFFBQVEsRUFBRSxDQUFDO1lBQ1gsT0FBTyxPQUFPLENBQUMsR0FBRyxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQ2pDLENBQUM7YUFBTSxDQUFDO1lBQ0osSUFBSSxRQUFRLEdBQUcsUUFBUSxDQUFDLGFBQWEsQ0FBQyxRQUFRLENBQUMsQ0FBQztZQUNoRCxRQUFRLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsQ0FBQztZQUNwQyxRQUFRLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQztRQUM1RCxDQUFDO0lBQ0wsQ0FBQyxDQUFDLENBQUM7SUFFSCxZQUFFLENBQUMsUUFBUSxDQUFDLGNBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFLElBQUksRUFBRSxLQUFLLEVBQUUsV0FBVyxDQUFDLEVBQUUsTUFBTSxFQUFFLFVBQVUsUUFBUSxFQUFFLFFBQVE7UUFFNUYsSUFBSSxRQUFRLEVBQUUsQ0FBQztZQUNYLE9BQU8sT0FBTyxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUNqQyxDQUFDO2FBQU0sQ0FBQztZQUNKLElBQUksUUFBUSxHQUFHLFFBQVEsQ0FBQyxhQUFhLENBQUMsUUFBUSxDQUFDLENBQUM7WUFDaEQsUUFBUSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsUUFBUSxDQUFDLENBQUM7WUFDcEMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUM7UUFDNUQsQ0FBQztJQUNMLENBQUMsQ0FBQyxDQUFDO0FBQ1AsQ0FBQyJ9