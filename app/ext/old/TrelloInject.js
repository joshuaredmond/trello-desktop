"use strict";
$(function () {
    setTimeout(function () {
        ZJS.Crx.Trello.trelloCardLinks();
        var tempInterval = window.setInterval(ZJS.Crx.Trello.trelloCardLinks, 100);
        setTimeout(function () {
            clearInterval(tempInterval);
        }, 2000);
        $(document.body)
            .on(`mouseup`, function () {
            setTimeout(ZJS.Crx.Trello.trelloCardLinks, 20);
            setTimeout(ZJS.Crx.Trello.trelloCardLinks, 1500);
        })
            .on(`keyup`, function () {
            setTimeout(ZJS.Crx.Trello.trelloCardLinks, 20);
        })
            .on(`click`, function () {
            setTimeout(ZJS.Crx.Trello.trelloCardLinks, 20);
        });
    }, 10);
});
//# sourceMappingURL=../dev/Crx/TrelloInject.js.map