"use strict";
var ZJS;
(function (ZJS) {
    let Crx;
    (function (Crx) {
        class Trello {
            // Based on https://github.com/oneezy/gtd-trello-card-links
            // Trello DOM element ids and classes changed 1 Oct 2023.
            static trelloCardLinks() {
                var $listJq = $(`div[data-testid='list']`);
                $(`.cardCounter_appsBrand`).remove();
                $(`.card-detail-window .js-fill-card-detail-desc .js-desc`).each(function (_i, descEl) {
                    var descHtml = $(descEl).html();
                    descHtml = ZJS.Crx.Trello.replaceLineNumbers(descHtml);
                    $(descEl).html(descHtml);
                });
                // Loop though card checklist items.
                $(`.js-checkitem-name`).each(function (_i, ticklistItemEl) {
                    var ticklistItemHtml = $(ticklistItemEl).html();
                    ticklistItemHtml = ZJS.Crx.Trello.replaceLineNumbers(ticklistItemHtml);
                    $(ticklistItemEl).html(ticklistItemHtml);
                });
                $listJq.each(ZJS.Crx.Trello.updateCards_html);
                $(`.cardPreviewLink_appsBrand:empty`).remove();
                $(`.cardPreviewLink_appsBrand`).on(`click`, function (evt) {
                    evt.stopPropagation();
                });
                $(`.openinphpstorm_appsBrand`).on(`click`, ZJS.Crx.Trello.openinphpstormOnclick); // Allow click or mouseup.
                ZJS.Crx.Trello.isSummaryFound = Boolean($(`.showSummary_appsBrand`).length);
                $listJq.each(ZJS.Crx.Trello.updateList);
                var listEl = $listJq.first().get(0);
                if (listEl) {
                    ZJS.Crx.Trello.insertButtonsOnFirstList(listEl);
                }
            }
            static openinphpstormOnclick(evt) {
                var _a;
                evt.preventDefault();
                var srcUrl = ((_a = evt.target) === null || _a === void 0 ? void 0 : _a.href.split(`:`)) || [];
                if (srcUrl.length == 3 && srcUrl[1]) {
                    var filePath = srcUrl[1].replace(/\\/g, `/`);
                    var fileLineNum = srcUrl[2] || `1`;
                    if (navigator.userAgent.includes(`Linux`)) {
                        // E.g., Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.80 Safari/537.36.
                        var locationUrl = `vscode://file`;
                        var fullNameAndUserName = $(`button[data-testid="board-facepile-member"]`).attr(`title`); // Allow .attr(`title`).
                        if (fullNameAndUserName === null || fullNameAndUserName === void 0 ? void 0 : fullNameAndUserName.includes(`ethanredmond2`)) {
                            locationUrl += `/home/ethanredmond`;
                        }
                        else if (fullNameAndUserName === null || fullNameAndUserName === void 0 ? void 0 : fullNameAndUserName.includes(`joshuaredmond57`)) {
                            locationUrl += `/home/joshuaredmond`;
                        }
                        locationUrl += `/mnt/ebs1/git/`;
                        if (!filePath.startsWith(`pouch/`) && !filePath.startsWith(`apps/`)) {
                            locationUrl += `apps/`;
                        }
                        locationUrl += `${filePath}:${fileLineNum}`;
                        location.href = locationUrl;
                    }
                    else if (navigator.userAgent.includes(`Windows`)) {
                        // E.g., Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36.
                        location.href = `openinphpstorm:(${fileLineNum})/${filePath}`;
                    }
                    else {
                        prompt(`Ctrl-C to copy, then Esc to close`, `/${filePath}:${fileLineNum}`); // Keep JavaScript prompt because it's in Trello.
                    }
                }
            }
            static updateCards_html(_ignoredCount, listEl) {
                var i = 1;
                $(listEl).find(`a[data-testid='card-name']`).each(function () {
                    // console.log($(this).html()); // Keep for debugging.
                    var cardHtml = $(this).html(); // Contains "<span class="card-short-id hide">#12355</span>" at the start.
                    var cardIdSpanHtml = (cardHtml.includes(`</span>`)) ? cardHtml.substring(0, cardHtml.indexOf(`</span>`) + 7) : ``;
                    var cardCounterHtml = `<div class="cardCounter_appsBrand">${i}</div>`;
                    cardHtml = cardHtml.replace(cardIdSpanHtml, ``);
                    cardHtml = ZJS.Crx.Trello.replaceLineNumbers(cardHtml);
                    cardHtml = ZJS.Crx.Trello.replaceCardOnly(cardHtml);
                    cardHtml = ZJS.Crx.Trello.replaceMarkdown(cardHtml);
                    $(this).html(cardIdSpanHtml + cardHtml + cardCounterHtml);
                    i++;
                });
            }
            static updateList(_ignoredCount, listEl) {
                var listHeading = $(listEl)
                    .find(`div[data-testid='list-header']`)
                    .first()
                    .find(`h2`)
                    .first()
                    .html();
                const listCardCount = $(listEl).find(`div[data-testid='trello-card'], div[data-testid='separator-card'], div[data-testid='minimal-card'], div[data-testid='full-cover-card'], a[data-testid='smart-links-container-layered-link']`).length;
                var listCardCountStr = (listCardCount == 1) ? `${listCardCount} card` : `${listCardCount} cards`;
                var $listCardCountJq = $(listEl).find(`p.listCardCount`);
                if ($listCardCountJq.length) {
                    $listCardCountJq.text(listCardCountStr);
                }
                else {
                    $(listEl)
                        .find(`div[data-testid='list-header']`)
                        .append(`<p class="listCardCount" style="margin: 0; line-height: 10px; opacity: 0.5; width: 100%; padding-right: 8px; font-size: 12px; text-align: right;">${listCardCountStr}</p>`);
                }
                var themeType = $(document.documentElement).data(`color-mode`);
                if (listHeading == `Done`) {
                    $(listEl)
                        .css({ 'background-color': (themeType == `dark`) ? `rgba(0, 0, 0, 0.6)` : `rgba(255, 255, 255, 0.4)` }) // Allow hard-coded rgba.
                        .find(`li[data-testid='list-card']`)
                        .css(`opacity`, `0.8`);
                    ZJS.Crx.Trello.insertButtonsOnFirstList(listEl);
                }
                else if (listHeading != `Doing`) {
                    $(listEl)
                        .find(`li[data-testid='list-card']`)
                        .css(`opacity`, `0.8`);
                }
            }
            static insertButtonsOnFirstList(listEl) {
                if (!ZJS.Crx.Trello.isSummaryFound) {
                    $(listEl)
                        .find(`button[data-testid='list-edit-menu-button']`)
                        .first()
                        .before(`<a href="" class="showSummary_appsBrand list-header-extras-menu dark-hover icon-sm icon-share" style="padding: 6px;" title="Show summary"></a><a href="" class="archiveCards_appsBrand list-header-extras-menu dark-hover icon-sm icon-archive" style="padding: 6px;" title="Archive all cards in this list"></a>`); // Allow title attribute. // Allow no stopEvt in onclick.
                    $(`.showSummary_appsBrand`).on(`click`, ZJS.Crx.Trello.showTrelloCardSummaryOnclick); // Allow click or mouseup.
                    $(`.archiveCards_appsBrand`).on(`click`, ZJS.Crx.Trello.archiveCardsOnclick); // Allow click or mouseup.
                    ZJS.Crx.Trello.isSummaryFound = true;
                }
            }
            static replaceLineNumbers(cardStr) {
                return cardStr
                    .replace(
                // Filenames without line number.
                /\b(\/?)((?:mod|pouch[\/\\]classes)[\/\\]\w+[\/\\][\w,]+[^\n:`"'<>]+?(?:\\.[a-z]+)?)(\n|<|`|'|"|$)/gi, `$1<a href="openinphpstorm:$2:1" class="cardPreviewLink_appsBrand openinphpstorm_appsBrand">$2</a>$3`)
                    .replace(
                // Filenames with line number.
                /\b(\/?)((?:mod|pouch[\/\\]classes)[\/\\]\w+[\/\\][\w,]+[^\n:"'<>]+)(\:(\d+))(\n|<|$)/gi, `$1<a href="openinphpstorm:$2:$4" class="cardPreviewLink_appsBrand openinphpstorm_appsBrand">$2$3</a>$5`)
                    .replace(
                // Filenames without line number.
                /\b(\/?)(classes[\/\\]PouchBase[\/\\]\w+[\/\\][\w,]+[^\n:"'<>]+?(?:\\.[a-z]+)?)(\n|<|$)/gi, // Allow PouchBase.
                `$1<a href="openinphpstorm:pouch/$2:1" class="cardPreviewLink_appsBrand openinphpstorm_appsBrand">$2</a>$3`)
                    .replace(
                // Filenames with line number.
                /\b(\/?)(classes[\/\\]PouchBase[\/\\]\w+[\/\\][\w,]+[^\n:"'<>]+)(\:(\d+))(\n|<|$)/gi, // Allow PouchBase.
                `$1<a href="openinphpstorm:pouch/$2:$4" class="cardPreviewLink_appsBrand openinphpstorm_appsBrand">$2$3</a>$5`)
                    .replace(
                // JavaScript ZJS functions/variables.
                /\b(ZJS\.[\w.]+)(\s|\(|,|<|$)/g, `<a href="" class="cardPreviewLink_appsBrand" onmousedown="prompt('Ctrl-C to copy, then Esc to close', '$1');" onclick="return false;" onauxclick="return false;">$1</a>$2` // Keep JavaScript prompt because it's in Trello. // Allow return because it's in Trello.
                )
                    .replace(
                // PHP classes (incl static function/property).
                /(\\[A-Z][\\\w]+\b(?:\:\:\$?\w+)?)(\s|\(|<|$)/g, function (_matchStr, $1, $2) {
                    return `<a href="" class="cardPreviewLink_appsBrand" onmousedown="prompt('Ctrl-C to copy, then Esc to close', '${$1.replace(/\\/g, `\\\\`)}');" onclick="return false;" onauxclick="return false;">${$1}</a>${$2}`; // Keep JavaScript prompt because it's in Trello. // Allow return because it's in Trello.
                });
            }
            static replaceMarkdown(cardStr) {
                return cardStr
                    .replace(
                // Backticks to <code>.
                /(^|[^`])`([^`]+)`([^`]|$)/gi, `$1<code class="code_appsBrand">$2</code>$3`)
                    .replace(
                // Rotate smileys.
                /(^|\W)([;:])(-?[()[\]{}pD])/gi, `$1<span class="smiley_appsBrand"><span>$2</span>$3</span>`)
                    .replace(
                // Asterisk to <b>.
                /(^|\s)\*([- \w()]+)\*(\s|$)/gi, `$1<b>$2</b>$3`)
                    .replace(
                // Underscore to <i>.
                /(^|\s)_([- \w()]+)_(\s|$)/gi, `$1<i>$2</i>$3`)
                    .replace(
                // Tilda to <strike>.
                /(^|\s)~([- \w()]+)~(\s|$)/gi, `$1<strike>$2</strike>$3`)
                    .replace(
                // Dash to arrow.
                / - /gi, ` &#8594; `);
            }
            static replaceCardOnly(cardStr) {
                return cardStr
                    .replace(
                // URLs.
                /\b((?:https?:\/\/)?\w+(?:\.\w+){1,4}\/[^\s"<>]*)(\s|$)/gi, `<a href="$1" target="_blank" class="cardPreviewLink_appsBrand">$1</a>$2`)
                    .replace(
                // Highlight: "fix"
                /^(fix:?)(\s)/gi, `<span class="spanFix_appsBrand">$1</span>$2`)
                    .replace(
                // Highlight person initials.
                /(^(([[A-Z]{2}\??)( ))|(( )([[A-Z]{2}\??))$)/g, `$6<span class="spanInitials_appsBrand">&nbsp;$3$7&nbsp;</span>$4`)
                    .replace(
                // Highlight keywords.
                /\b((?:applicant|batch|bookmark|location|timeSpot|opening|org|payableOnceOnly|payableRepeat|payee|paystub|person|project|projectRole|schedule|session|timeOff|timesheet|user)s?)\b([^-?.<&:"\/\\]|$)/gi, `<span class="spanKeywords_appsBrand">$1</span>$2`);
            }
            static archiveCardsOnclick(evt) {
                if (!ZJS.Crx.Trello.isArchiveCardsStarted && evt.target && $(evt.target).closest(`div[data-testid='list']`).find(`li[data-testid='list-card']`).length) {
                    ZJS.Crx.Trello.isArchiveCardsStarted = true;
                    var archiveCardsStartUnix = Date.now();
                    var archiveCardsJqInterval = window.setInterval(function () {
                        if (evt.target) {
                            var openListIconElsArr = $(evt.target)
                                .parent()
                                .find(`button[data-testid='list-edit-menu-button']`)
                                .get();
                            if (openListIconElsArr.length || (Date.now() - archiveCardsStartUnix) > 2000) {
                                clearInterval(archiveCardsJqInterval);
                            }
                            if (openListIconElsArr.length) {
                                openListIconElsArr[0].click(); // Have to use JavaScript method, not jQuery trigger().
                                archiveCardsJqInterval = window.setInterval(function () {
                                    var archiveCardsElsArr = $(`section[data-testid='list-actions-popover'] button:contains('Archive all cards in this list')`).get(); // Allow single quotes.
                                    if (archiveCardsElsArr.length || (Date.now() - archiveCardsStartUnix) > 2000) {
                                        clearInterval(archiveCardsJqInterval);
                                    }
                                    if (archiveCardsElsArr.length) {
                                        archiveCardsElsArr[0].click();
                                        archiveCardsJqInterval = window.setInterval(function () {
                                            var jsConfirmElsArr = $(`section[data-testid='list-actions-popover'] button:contains('Archive cards')`).get(); // Allow single quotes.
                                            if (jsConfirmElsArr.length || (Date.now() - archiveCardsStartUnix) > 2000) {
                                                clearInterval(archiveCardsJqInterval);
                                            }
                                            if (jsConfirmElsArr.length) {
                                                jsConfirmElsArr[0].click();
                                            }
                                        }, 30);
                                    }
                                }, 30);
                            }
                            ZJS.Crx.Trello.isArchiveCardsStarted = false;
                        }
                    }, 30);
                }
            }
            static showTrelloCardSummaryOnclick(evt) {
                clearTimeout(ZJS.Crx.Trello.showTrelloCardSummaryTimeout);
                // SetTimeout is to get around a weird bug that causes this function to fire 20 times.
                ZJS.Crx.Trello.showTrelloCardSummaryTimeout = window.setTimeout(function () {
                    if (evt !== undefined) {
                        if (evt.originalEvent !== undefined) {
                            evt.originalEvent.preventDefault();
                            evt.originalEvent.stopPropagation();
                        }
                        else {
                            evt.preventDefault();
                            evt.stopPropagation();
                        }
                    }
                    var summaryHtml = ZJS.Crx.Trello.summary_html();
                    // language=HTML
                    var closeButtonHtml = `<a href="" class="summaryClose_appsBrand"`; // Allow no stopEvt in onclick.
                    closeButtonHtml += `>[Close]</a>`;
                    var $summaryContainerJq = $(`<div class="summaryContainer_appsBrand">${closeButtonHtml}${summaryHtml}</div>`);
                    $(document.body).append($summaryContainerJq);
                    var firstSummaryListEl = $(`.summaryList_appsBrand`)
                        .on(`click`, function (evt) {
                        ZJS.Crx.Trello.selectSpan(evt.target);
                    })
                        .get(0);
                    if (firstSummaryListEl) {
                        ZJS.Crx.Trello.selectSpan(firstSummaryListEl);
                    }
                    $(`#surface, .summaryClose_appsBrand`).on(`click`, function () {
                        $(`.summaryContainer_appsBrand`).remove();
                    });
                }, 100);
            }
            static summary_html() {
                var summaryHtml = ``;
                $(`div[data-testid='list']`).each(function (_i, listEl) {
                    var listHeading = $(listEl)
                        .find(`div[data-testid='list-header']`)
                        .first()
                        .find(`h2`)
                        .first()
                        .html();
                    if (listHeading) {
                        summaryHtml += `${listHeading}<br>`;
                        summaryHtml += `<span class="summaryList_appsBrand">`;
                        $(listEl).find(`div[data-testid='trello-card'], div[data-testid='minimal-card']`).each(function (_j, cardEl) {
                            var cardHeading = ZJS.Crx.Trello.cardHeading(cardEl);
                            if (cardHeading) {
                                var timerText = ZJS.Crx.Trello.timerText(cardEl);
                                summaryHtml += `${$(cardEl).find(`.cardCounter_appsBrand`).html()}. `;
                                if (timerText != `0:00`) {
                                    summaryHtml += `${timerText} - `;
                                }
                                summaryHtml += `${cardHeading}<br>`;
                            }
                        });
                        summaryHtml += `</span><br>`;
                    }
                });
                return summaryHtml;
            }
            static cardHeading(cardEl) {
                var $cardHeadingJq = $(cardEl).find(`a[data-testid='card-name']`).first();
                var cardHeading = $cardHeadingJq.text();
                if (cardHeading) {
                    if (cardHeading.startsWith(`#`)) {
                        var cardId = $cardHeadingJq
                            .find(`span`)
                            .first()
                            .text();
                        cardHeading = cardHeading.replace(cardId, ``);
                    }
                    var cardCountStr = $cardHeadingJq
                        .find(`.cardCounter_appsBrand`)
                        .last()
                        .text();
                    if (cardCountStr) {
                        cardHeading = cardHeading.replace(new RegExp(`${cardCountStr}$`), ``);
                    }
                    return cardHeading;
                }
                else {
                    return ``;
                }
            }
            static timerText(cardEl) {
                var timerBadgeText = $(cardEl)
                    .find(`.js-plugin-badges`)
                    .find(`.badge-text`)
                    .first()
                    .text();
                timerBadgeText || (timerBadgeText = `0h 0m`);
                if (!timerBadgeText.includes(`m`)) {
                    timerBadgeText += ` 0m`;
                }
                var badgeTextArr = timerBadgeText.replace(`m`, ``).split(`h `);
                if (badgeTextArr.length == 1) {
                    badgeTextArr.unshift(`0`);
                }
                if (parseInt(badgeTextArr[1]) < 10) {
                    badgeTextArr[1] = `0${badgeTextArr[1]}`;
                }
                return badgeTextArr.join(`:`);
            }
            static selectSpan(srcEl) {
                var rangeObj = document.createRange();
                rangeObj.selectNodeContents(srcEl);
                var selectionObj = window.getSelection();
                if (selectionObj) {
                    selectionObj.removeAllRanges();
                    selectionObj.addRange(rangeObj);
                }
            }
        }
        Trello.isArchiveCardsStarted = false;
        Trello.isSummaryFound = false;
        Trello.showTrelloCardSummaryTimeout = 0;
        Crx.Trello = Trello;
    })(Crx = ZJS.Crx || (ZJS.Crx = {}));
})(ZJS || (ZJS = {}));
//# sourceMappingURL=../dev/Crx/Trello.js.map